<?php
namespace Mail\Mail;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;
use Zend\Mime\Mime;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class MailService
{
	protected $smtpOptions;
	protected $from;
	protected $enable;
	
	public function __construct($options)
	{
		if ($options['enable'] === true)
		{
			$this->smtpOptions   = new SmtpOptions($options['smtp_options']);
			$this->from = array($options['mail_address'] => $options['display_name']);
			$this->enable = true;
		}
		else
			$this->enable = false;
	}
	
	public function send($message)
	{
		if ($this->enable !== true)
			return;
		
		$mimeMessage = new MimeMessage();
		
		$messageText = new Part($message['body']);
		$messageText->type = 'text/html';
		$messageText->charset = 'utf8';
		$parts = array($messageText);
		
		if (isset($message['attachments']))
			foreach ($message['attachments'] as $attachment)
			{
				$data = fopen($filename = "data/uploads/" . $attachment['id'], 'r');
				$messageAttachment = new Part($data);
				$messageAttachment->filename = $attachment['name'];
				$messageAttachment->encoding = Mime::ENCODING_BASE64;
				$messageAttachment->disposition = Mime::DISPOSITION_ATTACHMENT;
				array_push($parts, $messageAttachment);
			}
		
		$mimeMessage->setParts($parts);
		
		$mail = new Message();
		$mail->setSubject($message['subject']);
		$mail->setBody($mimeMessage);
		$mail->setFrom($this->from);
		$mail->setEncoding('utf8');
		foreach ($message['to'] as $user)
			if ($user['email'] !== null)
				$mail->addTo($user['email'], $user['name']);
		if (isset($message['cc']) && is_array($message['cc']))
			foreach ($message['cc'] as $user)
				if ($user['email'] !== null)
					$mail->addCC($user['email'], $user['name']);
		
		$transport = new SmtpTransport();
		$transport->setOptions($this->smtpOptions);
		$transport->send($mail);
	}
}