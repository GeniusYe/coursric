<?php
return array(
	'mail' => array(
		'enable' => false,
	),
	'service_manager' => array(
		'factories' => array(
			'mail_service' => 'Mail\Service\Factory\Mail',
		),
	),
);
