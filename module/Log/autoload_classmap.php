<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'Log\Module'                             => __DIR__ . '/Module.php',
  'Log\Db\LogService'                      => __DIR__ . '/src/Log/Db/LogService.php',
  'Log\Model\Log'                          => __DIR__ . '/src/Log/Model/Log.php',
  'Log\Service\Factory\Log'                => __DIR__ . '/src/Log/Service/Factory/Log.php',
  'Log\Service\Factory\LogDatabaseAdapter' => __DIR__ . '/src/Log/Service/Factory/LogDatabaseAdapter.php',
);
