<?php

namespace Log\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Log\Db\LogService;

class Log implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('log_database_adapter');
		$log_service = new LogService($db_adapter);
		return $log_service;
	}
}

?>