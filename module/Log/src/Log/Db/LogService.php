<?php

namespace Log\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Log\Model\Log as LogModel;

class LogService
{
	private $db_adapter;
	private $log_model;
	
	const INFO = 2;
	const SENSITIVE = 3;
	const REALLY_SENSITIVE = 4;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->log_model = new LogModel($this->db_adapter);
	}
	
	public function log($module_id, $operation, $user_id, $level, $optional_data = null)
	{
		if ($optional_data === null)
			$this->log_model->insert(
				array(
					'module_id' => $module_id, 
					'operation' => $operation, 
					'user_id' => $user_id, 
					'level' => $level,
				)
			);
		else
			$this->log_model->insert(
				array(
					'module_id' => $module_id, 
					'operation' => $operation, 
					'user_id' => $user_id, 
					'level' => $level,
					'optional_id' => isset($optional_data['optional_id']) ? $optional_data['optional_id'] : null,
					'optional_str' => isset($optional_data['optional_str']) ? $optional_data['optional_str'] : null,
					'short_info' => isset($optional_data['short_info']) ? $optional_data['short_info'] : null,
					'long_info' => isset($optional_data['long_info']) ? $optional_data['long_info'] : null,
				)
			);
	}
}

?>