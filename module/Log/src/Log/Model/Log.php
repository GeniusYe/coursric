<?php

namespace Log\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class Log extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "log";
		$this->adapter = $adapter;
	}
}