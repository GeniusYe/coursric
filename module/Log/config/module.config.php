<?php
return array(
    'view_manager' => array(
        'template_path_stack' => array(
            'Log' => __DIR__ . '/../view',
        ),
    ),
	'service_manager' => array(
		'factories' => array(
			'log_database_adapter' => 'Log\Service\Factory\LogDatabaseAdapter',
			'log_service' => 'Log\Service\Factory\Log',
		),
	),
);
