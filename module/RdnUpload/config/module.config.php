<?php

return array(
	'controllers' => array(
		'invokables' => array(
			'RdnUpload\Controller\File' => 'RdnUpload\Controller\FileController',
		),
	),
	'router' => array(
		'routes' => array(
			'files' => array(
				'type'    => 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'    => '/files',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'RdnUpload\Controller',
						'controller'    => 'File',
						'action'        => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
		
	'controller_plugins' => array(
		'aliases' => array(
			'uploads' => 'RdnUpload:Uploads',
		),

		'factories' => array(
			'RdnUpload:Uploads' => 'RdnUpload\Factory\Controller\Plugin\Uploads',
		),
	),

	'rdn_upload' => array(
		'adapter' => 'Filesystem',
		'temp_dir' => null,
	),

	'rdn_upload_adapters' => array(
		'factories' => array(
			'Filesystem' => 'RdnUpload\Factory\Adapter\Filesystem',
			'Gaufrette' => 'RdnUpload\Factory\Adapter\Gaufrette',
		),

		'configs' => array(
			'Filesystem' => array(
				'upload_path' => 'data/uploads',
				'public_path' => '/files',
			),

			'Gaufrette' => array(
				'filesystem' => null,
				'public_path' => null,
			),
		),
	),

	'service_manager' => array(
		'factories' => array(
			'RdnUpload\Adapter\AdapterManager' => 'RdnUpload\Factory\Adapter\AdapterManager',
			'RdnUpload\Container' => 'RdnUpload\Factory\Container',
			'stored_file_service' => 'RdnUpload\Service\Factory\StoredFileControl',
		),
	),
		
    'view_manager' => array(
        'template_path_stack' => array(
            'RdnUpload' => __DIR__ . '/../view',
        ),
    ),

	'view_helpers' => array(
		'aliases' => array(
			'uploads' => 'RdnUpload:Uploads',
		),

		'factories' => array(
			'RdnUpload:Uploads' => 'RdnUpload\Factory\View\Helper\Uploads',
		),
	),
);
