<?php

namespace RdnUpload\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RdnUpload\Model\StoredFileFree as StoredFileFreeModel;
use RdnUpload\Model\StoredFilePrivilege as StoredFilePrivilegeModel;
use RdnUpload\Util\ErrorType;

/**
 * FileController
 *
 * @author
 *
 * @version
 *
 */
class FileController extends AbstractActionController 
{
	public function uploadAction()
	{
		$id = $this->uploads()->upload($_FILES['file']);
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$stored_file_free_model = new StoredFileFreeModel($db_adapter);
		$stored_file_free_model->insert(array('id' => $id));
		
		$object = $this->uploads()->get($id);

		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/ajax');
		$viewModel->setVariables(array(
				'result' => array(
						'result' => true,
						'id' => $id,
						'name' => $_FILES['file']['name'],
				)
		));
		$viewModel->setTerminal(true);
		return $viewModel;
	}
	
	public function downloadAction()
	{
		$id = $_REQUEST['id'];

		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$stored_file_privilege_model = new StoredFilePrivilegeModel($db_adapter);
		
		$privileges_result = $stored_file_privilege_model->select(array('stored_file_id' => $id));
		
		$t = false;
		$auth = $this->getServiceLocator()->get('auth');
		$storage = $auth->getStorage()->read();
		foreach ($privileges_result as $privilege)
		{
			switch ($privilege["confidential_level"])
			{
				case 'PUBLIC':
					$t = true;
					break;
				case 'TA_ONLY':
					$course_id = $privilege["course_id"];
					if ((is_array($storage['ta_course_list']) && in_array($course_id, $storage['ta_course_list'])) ||
						(is_array($storage['teacher_course_list']) && in_array($course_id, $storage['teacher_course_list'])))
						$t = true;
					break;
				case 'COURSE_PUBLIC':
					$course_id = $privilege["course_id"];
					if ((is_array($storage['ta_course_list']) && in_array($course_id, $storage['ta_course_list'])) ||
						(is_array($storage['teacher_course_list']) && in_array($course_id, $storage['teacher_course_list'])) ||
						(is_array($storage['stu_course_list']) && in_array($course_id, $storage['stu_course_list'])))
						$t = true;
					break;
				case 'PERSONAL':
					$user_id = $privilege["user_id"];
					if (isset($storage['user']) && $storage['user']['id'] == $user_id)
						$t = true;
					break;
			}
			if ($t)
				break;
		}
		if (!$t)
		{
			$response = $this->getResponse();
			$response->setStatusCode(401);
			
			$viewModel = new ViewModel();
    		$viewModel->setTemplate('rdn-upload/error');
    		$viewModel->setVariables(array('reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
			$viewModel->setTerminal(true);
			return $viewModel;
		}
		
		$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : "";
		
		return $this->uploads()->getResponse($id, $name);
	}
}