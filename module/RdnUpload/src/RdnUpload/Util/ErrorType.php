<?php
namespace RdnUpload\Util;

class ErrorType
{
	const NOT_AUTHENTICATED = 0;
	const NOT_ENOUGH_RIGHTS = 1;
}