<?php
namespace RdnUpload\Service;

use RdnUpload\Model\StoredFileFree as StoredFileFreeModel;
use RdnUpload\Model\StoredFileRegistered as StoredFileRegisteredModel;
use RdnUpload\Model\StoredFilePrivilege as StoredFilePrivilegeModel;
use RdnUpload\Util\ErrorType;

class StoredFileService
{
	protected $stored_file_registered_model;
	protected $stored_file_free_model;
	protected $stored_file_privilege_model;
	
	public function __construct(
			StoredFileFreeModel $stored_file_free_model, 
			StoredFileRegisteredModel $stored_file_registered_model,
			StoredFilePrivilegeModel $stored_file_privilege_model)
	{
		$this->storedFileFreeModel = $stored_file_free_model;
		$this->storedFileRegisteredModel = $stored_file_registered_model;
		$this->storedFilePrivilege = $stored_file_privilege_model;
	}
	
	public function saveFile($id, $stored_file_privilege = array('confidential_level' => 'PUBLIC'))
	{
		$stored_file_privilege['stored_file_id'] = $id;
		$ori_file_result = $this->storedFileRegisteredModel->select(array('id' => $id));
		$ori_file = $ori_file_result->current();
		if ($ori_file === false)
		{
			$free_file_result = $this->storedFileFreeModel->select(array('id' => $id));
			$free_file = $free_file_result->current();
			if ($free_file === null)
				return array('result' => false, 'reason' => ErrorType::FILE_DOES_NOT_UPLOADED);
			$this->storedFileRegisteredModel->insert(array('id' => $id, 'reference' => 1));
			$this->storedFileFreeModel->delete(array('id' => $id));
		}
		else
		{
			$this->storedFileRegisteredModel->update(array('reference' => $ori_file['reference'] + 1), array('id' => $id));
		}
		$this->storedFilePrivilege->insert($stored_file_privilege);
		return array('result' => true);
	}

	public function deleteFile($id, $stored_file_privilege = array('confidential_level' => 'PUBLIC'))
	{
		$stored_file_privilege['stored_file_id'] = $id;
		$ori_file_result = $this->storedFileRegisteredModel->select(array('id' => $id));
		$ori_file = $ori_file_result->current();
		if ($ori_file !== false)
		{
			$this->storedFilePrivilege->deleteOne($stored_file_privilege);
			if ($ori_file['reference'] - 1 > 0)
				$this->storedFileRegisteredModel->update(array('reference' => $ori_file['reference'] - 1), array('id' => $id));
			else
			{
				$this->storedFileFreeModel->insert(array('id' => $id));
				$this->storedFilePrivilege->delete(array('stored_file_id' => $id));
				$this->storedFileRegisteredModel->delete(array('id' => $id));
			}
		}
		return array('result' => true);
	}
}