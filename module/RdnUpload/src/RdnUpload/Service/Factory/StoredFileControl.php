<?php

namespace RdnUpload\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use RdnUpload\Model\StoredFileFree as StoredFileFreeModel;
use RdnUpload\Model\StoredFileRegistered as StoredFileRegisteredModel;
use RdnUpload\Model\StoredFilePrivilege as StoredFilePrivilegeModel;
use RdnUpload\Service\StoredFileService;

class StoredFileControl implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$stored_file_free_model = new StoredFileFreeModel($db_adapter);
		$stored_file_registered_model = new StoredFileRegisteredModel($db_adapter);
		$stored_file_privilege_model = new StoredFilePrivilegeModel($db_adapter);
		$storedFileService = new StoredFileService($stored_file_free_model, $stored_file_registered_model, $stored_file_privilege_model);
		return $storedFileService;
	}
}

?>