<?php

namespace RdnUpload\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class StoredFileRegistered extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "stored_file_registered";
		$this->adapter = $adapter;
	}
}