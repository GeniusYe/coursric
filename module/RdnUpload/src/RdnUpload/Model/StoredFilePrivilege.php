<?php

namespace RdnUpload\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class StoredFilePrivilege extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "stored_file_privilege";
		$this->adapter = $adapter;
	}
	
	public function deleteOne($where)
	{
		$ori_privilege_result = $this->select($where);
		$ori_privilege = $ori_privilege_result->current();
		if ($ori_privilege !== false)
		{
			$this->delete(array('id' => $ori_privilege['id']));
		}
	}
}