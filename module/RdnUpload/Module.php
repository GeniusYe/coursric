<?php

namespace RdnUpload;

use Zend\Mvc\MvcEvent;

class Module
{
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}
	
	public function getAutoloaderConfig()
    {
        return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php',
			),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }
    public function onBootstrap(MvcEvent $e)
    {
    	// You may not need to do this if you're doing it elsewhere in your
    	// application
    	$eventManager        = $e->getApplication()->getEventManager();
    
    	$eventManager = $e->getApplication()->getEventManager();
    	$eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'protectPage'), -100);
    }
    
    public function protectPage(MvcEvent $event)
    {
    	$match = $event->getRouteMatch();
    	if(!$match) {
    		// we cannot do anything without a resolved route
    		return;
    	}
    
    	$controller = $match->getParam('controller');
    	$action     = $match->getParam('action');
    	$namespace  = $match->getParam('__NAMESPACE__');
    
    	$services = $event->getApplication()->getServiceManager();
    	$auth = $services->get('auth');
    	 
    	$auth_t = true;
    	 
    	if (strpos($controller, 'RndUpload') === 0)
    	{
	    	if (!$auth->hasIdentity())
	    	{
	    		if ($controller === 'RndUpload\Controller\File')
	    			$auth_t = false;
	    	}
    	}
    	 
    	if ($auth_t == false)
    	{
    		// Set the response code to HTTP 401: Auth Required
    		$response = $event->getResponse();
    		$response->setStatusCode(401);
    
    		$match->setParam('controller', 'User\Controller\Log');
    		$match->setParam('action', 'in');
    	}
    }
}
