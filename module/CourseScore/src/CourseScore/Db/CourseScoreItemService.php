<?php

namespace CourseScore\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use CourseScore\Model\CourseScoreItem as CourseScoreItemModel;
use CourseScore\Util\ServiceUtil;
use CourseScore\Util\ControllerUtil;
use CourseScore\Util\ErrorType;

class CourseScoreItemService
{
	private $db_adapter;
	private $course_score_item_model;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->course_score_item_model = new CourseScoreItemModel($this->db_adapter);
	}
	
	public function query_by_id($id)
	{
		$course_score_item_result = $this->course_score_item_model->select(array('id' => $id));
		$course_score_item = $course_score_item_result->current();
		return $course_score_item;
	}
	
	public function query_by_course_id($course_id)
	{
		$course_score_item_result = $this->course_score_item_model->select(array('course_id' => $course_id));
		$course_score_item_list = array();
		foreach ($course_score_item_result as $course_score_item)
			array_push($course_score_item_list, $course_score_item);
		return $course_score_item_list;
	}
	
	/**
	 * @param $data:
	 * 		abstract
	 * 		course_id
	 * 		min_score
	 * 		max_score
	 * 		weight
	 *
	 * @return multitype:boolean multitype:unknown  |number
	 */
	public function insert($data)
	{
		$this->course_score_item_model->insert(array(
			'abstract' => $data['abstract'],
			'course_id' => $data['course_id'],
			'min_score' => $data['min_score'],
			'max_score' => $data['max_score'],
			'weight' => $data['weight'],
			'publicity_level' => 0,
		));
	
		$id = $this->course_score_item_model->getLastInsertValue();
	
		return array('result' => true, 'id' => $id);
	}
	
	/**
	 * @param $data:
	 * 		id
	 * 		abstract
	 * 		min_score
	 * 		max_score
	 * 		weight
	 *
	 * @return multitype:boolean multitype:unknown  |number
	 */
	public function update($id, $data)
	{
		$course_score_item = $this->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
		
		$format = array(
			'abstract' => array('type' => ServiceUtil::RAW),
			'min_score' => array('type' => ServiceUtil::RAW),
			'max_score' => array('type' => ServiceUtil::RAW),
			'weight' => array('type' => ServiceUtil::RAW),
		);
		
		$update_data = ServiceUtil::generate_update_data(
			$format, 
			array(
				'abstract' => $data['abstract'],
				'min_score' => $data['min_score'],
				'max_score' => $data['max_score'],
				'weight' => $data['weight'],
			),
			$course_score_item
		);
		
		$this->course_score_item_model->update(
			$update_data,
			array('id' => $id)
		);
	
		return array('result' => true);
	}
	
	/**
	 * @param $data:
	 * 		id
	 * 		abstract
	 * 		min_score
	 * 		max_score
	 *
	 * @return multitype:boolean multitype:unknown  |number
	 */
	public function publish($id)
	{
		$this->course_score_item_model->update(
			array(
				'publicity_level' => 1,
			),
			array('id' => $id)
		);
	
		return array('result' => true);
	}
	
	public function delete($id)
	{
		$this->db_adapter->query('DELETE FROM course_stu_score WHERE course_score_item_id = ?', array($id));
		$this->course_score_item_model->delete(array('id' => $id));
		
		return array('result' => true);
	}
}

?>