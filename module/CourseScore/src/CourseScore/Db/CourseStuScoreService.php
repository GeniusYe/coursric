<?php

namespace CourseScore\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use CourseScore\Model\CourseStuScore as CourseStuScoreModel;

class CourseStuScoreService
{
	private $db_adapter;
	private $course_stu_score_model;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->course_stu_score_model = new CourseStuScoreModel($this->db_adapter);
	}
	
	/**
	 * course_id must match course_material_id, otherwise this would cause unexpected behavior
	 * @param unknown $course_id
	 * @param unknown $course_material_id
	 */
	public function query_stu_score_by_course_and_score_item($course_score_item_id, $course_id)
	{
		$stu_score_result = $this->course_stu_score_model->query_stu_score_by_course_and_score_item($course_score_item_id, $course_id);
		$stu_score_list = array();
		foreach ($stu_score_result as $stu_score)
			array_push($stu_score_list, $stu_score);
		return $stu_score_list;
	}
	
	/**
	 * Query all the scores that one student got in one course
	 * @param unknown $user_id
	 * @param unknown $course_id
	 */
	public function query_stu_score_by_course($user_id, $course_id)
	{
		$stu_score_result = $this->course_stu_score_model->query_stu_score_by_course($user_id, $course_id);
		$stu_score_list = array();
		foreach ($stu_score_result as $stu_score)
			array_push($stu_score_list, $stu_score);
		return $stu_score_list;
	}
	
	/**
	 * Query all the scores that one student got in one course
	 * @param unknown $user_id
	 * @param unknown $course_id
	 */
	public function query_stu_score_by_course_from_ta($user_id, $course_id)
	{
		$stu_score_result = $this->course_stu_score_model->query_stu_score_by_course_from_ta($user_id, $course_id);
		$stu_score_list = array();
		foreach ($stu_score_result as $stu_score)
			array_push($stu_score_list, $stu_score);
		return $stu_score_list;
	}
	
	public function merge_score($course_score_item_id, $user_id, $value, $rank)
	{
		$this->course_stu_score_model->merge_score($course_score_item_id, $user_id, $value, $rank);
		return array('result' => true);
	}
}

?>