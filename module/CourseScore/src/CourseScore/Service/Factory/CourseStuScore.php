<?php

namespace CourseScore\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CourseScore\Db\CourseStuScoreService;

class CourseStuScore implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$course_stu_score_service = new CourseStuScoreService($db_adapter);
		return $course_stu_score_service;
	}
}

?>