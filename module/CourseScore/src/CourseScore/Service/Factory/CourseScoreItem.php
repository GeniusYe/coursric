<?php

namespace CourseScore\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CourseScore\Db\CourseScoreItemService;

class CourseScoreItem implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$course_score_item_service = new CourseScoreItemService($db_adapter);
		return $course_score_item_service;
	}
}

?>