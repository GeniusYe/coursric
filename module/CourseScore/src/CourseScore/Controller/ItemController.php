<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/CourseScore for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CourseScore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\Json\Json;
use Log\Db\LogService;
use CourseScore\Util\ControllerUtil;
use CourseScore\Util\ErrorType;
use CourseScore\Util\MailTemplate;

class ItemController extends AbstractActionController
{
	public function queryAction()
	{
		$course_id = $this->params('id');
		
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
			
		/* Start Query */

		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		
		$course_score_items = $course_score_item_service->query_by_course_id($course_id);
		
		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true && $auth_status['admin']!== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Rendering */
		
		$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score_item_setting'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-score/item/query');
		$viewModel->setVariables(array('course_id' => $course_id, 'auth_status' => $auth_status, 'course_score_items' => $course_score_items));
		
		return $viewModel;
	}
	
	public function queryOneAction()
	{
		$course_score_item_id = $this->params('id');
		
		if (is_numeric($course_score_item_id))
		{
			$db_adapter = $this->getServiceLocator()->get('database_adapter');
			
			$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
			
			/* Actual Query */
			
			$course_score_item = $course_score_item_service->query_by_id($course_score_item_id);
			if ($course_score_item === false)
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
		
			/* Authorization Check */
		
			$course_id = $course_score_item['course_id'];
			$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
			if ($auth_status['is_authenticated'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
			if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
			
			/* Rendering */
			
			$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score_item_setting'));
			return array(
				'course_score_item' => $course_score_item,
				'course_id' => $course_id
			);
		}
		else
		{
			if (isset($_GET['course_id']))
				$course_id = $_GET['course_id'];
			else
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
			
			/* Rendering */
			
			$empty = array(
				'id' => 'new',
				'abstract' => '',
				'course_id' => $course_id,
				'min_score' => 0,
				'max_score' => 100,
				'weight' => 0,
				'publicity_level' => 0,
			);
			
			$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score_item_setting'));
			
			return array(
				'course_score_item' => $empty,
				'course_id' => $course_id
			);
		}
	}
	
	public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'abstract' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'course_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'min_score' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			'max_score' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			'weight' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
	
		$result = false;
	
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
	
		/* Start Query */
	
		$id = $filter->getValue('id');
	
		if ($id === 'new')
		{
			$course_id = $filter->getValue('course_id');
		}
		else if (is_numeric($id))
		{
			$course_score_item = $course_score_item_service->query_by_id($id);
			if ($course_score_item === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
			$course_id = $course_score_item['course_id'];
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
	
		/* Authorization Check */
	
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
	
		$min_score = $filter->getValue('min_score') * 10;
		$max_score = $filter->getValue('max_score') * 10;
		$weight = $filter->getValue('weight') * 10;
		
		if ($id === 'new')
		{
			$result = $course_score_item_service->insert(array(
				'abstract' => $filter->getValue('abstract'),
				'course_id' => $filter->getValue('course_id'),
				'min_score' => $min_score,
				'max_score' => $max_score,
				'weight' => $weight,
				'publicity_level' => 0,
			));
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else 
				$id = $result['id'];
	
			$notification_operation = 'add';
				
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $course_score_item_service->update($id, array(
				'abstract' => $filter->getValue('abstract'),
				'min_score' => $min_score,
				'max_score' => $max_score,
				'weight' => $weight,
			));
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$notification_operation = 'edit';
				
			$result = true;
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		$course_info_service->update ($course_id, array('last_schedule_modify' => date('Y-m-d', time())));
	
		$db_adapter->getDriver()->getConnection()->commit();
	
		$course_score_item = $course_score_item_service->query_by_id($id);
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		if ($notification_operation === 'add')
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_ADD, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Create score item with name ' . $course_score_item['abstract'] . ' under course ' . $course['name'] . '(' . $course['id'] . ')'));
		else if ($notification_operation === 'edit')
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_EDIT, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Update score item with new name ' . $course_score_item['abstract']));
		
		/* Rendering */
	
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
	
	public function deleteAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
		
		/* Start Query */

		$course_score_item = $course_score_item_service->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
		
		/* Authorization Check */
		
		$course_id = $course_score_item['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge */
		
		$result = $course_score_item_service->delete($id);
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_DELETE, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id));
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	public function publishAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
			
		/* Start Query */

		$course_score_item = $course_score_item_service->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
		
		$course = $course_service->query_by_id($course_score_item['course_id']);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		
		/* Authorization Check */
		
		$course_id = $course_score_item['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge */
		
		$result = $course_score_item_service->publish($id);
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$result = true;
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_PUBLISH, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id));
		
		/* Notification */
		
		$course_info = $course_info_service->query_by_id($course_score_item['course_id']);
		
		$mail_list = ControllerUtil::get_course_mail_list($course_service, $course_id);
			
		$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
		$this->send_notification(
			array(
				'course_info' => $course_info,
				'course_score_item' => $course_score_item,
				'mail_list' => $mail_list,
			),
			$app_name_config
		);
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_NOTIFY, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id));
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
	
	public function notifyAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
	
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
			
		/* Start Query */

		$course_score_item = $course_score_item_service->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
	
		$course = $course_service->query_by_id($course_score_item['course_id']);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
	
		/* Authorization Check */
	
		$course_id = $course_score_item['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
	
		/* Notification */
		
		$course_info = $course_service->query_by_id($course_score_item['course_id']);

		$mail_list = ControllerUtil::get_course_mail_list($course_service, $course_id);
			
		$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
		$this->send_notification(
			array(
				'course_info' => $course_info,
				'course_score_item' => $course_score_item,
				'mail_list' => $mail_list,
			),
			$app_name_config
		);
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_NOTIFY, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id));
	
		/* Rendering */
	
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true,
		));
	}
	
	private function send_notification($data, $app_name_config)
	{
		$course_info = $data['course_info'];
		$course_info_data = Json::decode($course_info['data'], true);
		$course_score_item = $data['course_score_item'];
	
		$format = MailTemplate::SCORE_PUBLISH_TEMPLATE;
		$subject = 'New Course Score Released On "' . $course_info_data['name'] . '" [' . $app_name_config['name_display'] . ']';
	
		$body = MailTemplate::HTML_TEMPLATE .
			sprintf($format,
				$course_info_data['name'],
				$course_score_item['abstract'],
				$app_name_config['domain_name'],
				$course_score_item['course_id'],
				$app_name_config['domain_name'],
				$course_score_item['course_id'],
				$app_name_config['name_display'],
				$app_name_config['domain_name']
			);
	
		$mail_service = $this->getServiceLocator()->get('mail_service');
		$mail_service->send(array(
			'subject' => $subject,
			'body' => $body,
			'to' => $data['mail_list']['to_list'],
			'cc' => $data['mail_list']['cc_list'],
			'attachments' => array(),
		));
	}
}
