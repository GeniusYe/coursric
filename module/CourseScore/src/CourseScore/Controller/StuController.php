<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/CourseScore for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CourseScore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CourseScore\Util\ControllerUtil;
use CourseScore\Util\ErrorType;

class StuController extends AbstractActionController
{
	public function queryAction()
	{
		$course_id = $this->params('id');
		
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_stu_score_service = $this->getServiceLocator()->get('course_stu_score_service');
			
		/* Start Query */

		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));

		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['stu'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$user_id = $auth_status['user']['id'];
		
		/* ActualQuery */
		
		$scores = $course_stu_score_service->query_stu_score_by_course($user_id, $course_id);
		
		/* Rendering */
		
		$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-score/stu/query');
		$viewModel->setVariables(array('scores' => $scores));
		
		return $viewModel;
	}
}
