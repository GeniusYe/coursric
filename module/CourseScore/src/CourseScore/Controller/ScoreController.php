<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/CourseScore for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CourseScore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Log\Db\LogService;
use CourseScore\Util\ControllerUtil;
use CourseScore\Util\ErrorType;

class ScoreController extends AbstractActionController
{
	public function queryAction()
	{
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
		$course_stu_score_service = $this->getServiceLocator()->get('course_stu_score_service');
			
		/* Start Query */

		$course_score_item = $course_score_item_service->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));

		$course_id = $course_score_item['course_id'];
		
		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true && $auth_status['admin']!== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* ActualQuery */
		
		$course_stu_scores = $course_stu_score_service->query_stu_score_by_course_and_score_item($course_score_item['id'], $course_id);
		
		/* Rendering */
		
		$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score_item_setting'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-score/score/query');
		$viewModel->setVariables(array('course_score_item' => $course_score_item, 'course_stu_scores' => $course_stu_scores));
		
		return $viewModel;
	}
	
	public function updateAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
		$course_stu_score_service = $this->getServiceLocator()->get('course_stu_score_service');
		
		/* Start Query */
	
		$course_score_item = $course_score_item_service->query_by_id($id);
		if ($course_score_item === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_SCORE_ITEM_DOES_NOT_EXIST)));
	
		$course_id = $course_score_item['course_id'];
	
		/* Authorization Check */
	
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
	
		/* Merge */
	
		$course_service = $this->getServiceLocator()->get('course_service');
		
		$course_score_item_id = $course_score_item['id'];
		
		$stu_list = $course_service->query_stu_list($course_id);
		$data = $this->getRequest()->getPost();
		foreach ($stu_list as $stu)
		{
			$user_id = $stu['user_id'];
			if (isset($data[$user_id]) && is_numeric(trim($data[$user_id])))
			{
				$score = $data[$user_id] * 10;
				$result = $course_stu_score_service->merge_score($course_score_item_id, $user_id, $score, null);
				if ($result['result'] !== true)
					return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			}
		}
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::ITEM_SCORE, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_score_item_id));
	
		/* Rendering */
	
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	public function queryAllAction()
	{
		$course_id = $this->params('id');
		
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_service = $this->getServiceLocator()->get('course_service');
		$course_score_item_service = $this->getServiceLocator()->get('course_score_item_service');
		$course_stu_score_service = $this->getServiceLocator()->get('course_stu_score_service');
		
		/* Start Query */

		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		
		/* Authorization Check */
	
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true && $auth_status['admin']!== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Query */
		
		$course_score_item_list = $course_score_item_service->query_by_course_id($course_id);

		$stu_list = $course_service->query_stu_list($course_id);

		$course_stu_scores = array();
		foreach ($stu_list as $stu)
		{
			$course_stu_scores[$stu['user_id']] = array('name' => $stu['name'], 'score' => $course_stu_score_service->query_stu_score_by_course_from_ta($stu['user_id'], $course_id));
		}

		/* Rendering */
		
		$this->layout()->setTemplate('course-score/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'score_item_setting'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-score/score/query-all');
		$viewModel->setVariables(array('course_score_item_list' => $course_score_item_list, 'course_stu_scores' => $course_stu_scores));
		
		return $viewModel;
	}
}
