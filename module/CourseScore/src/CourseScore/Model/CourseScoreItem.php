<?php

namespace CourseScore\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class CourseScoreItem extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course_score_item";
		$this->adapter = $adapter;
	}
}