<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'CourseScore\Controller\Item' => 'CourseScore\Controller\ItemController',
			'CourseScore\Controller\Score' => 'CourseScore\Controller\ScoreController',
			'CourseScore\Controller\Stu' => 'CourseScore\Controller\StuController',
		),
	),
	'router' => array(
		'routes' => array(
			'course-score' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/course_score',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'CourseScore\Controller',
// 						'controller'	=> 'Index',
// 						'action'		=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'		 => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_map' => include __DIR__  . '/../template_map.php',
		'template_path_stack' => array(
			'CourseScore' => __DIR__ . '/../view',
		),
	),
	'service_manager' => array(
		'factories' => array(
			'course_score_item_service' => 'CourseScore\Service\Factory\CourseScoreItem',
			'course_stu_score_service' => 'CourseScore\Service\Factory\CourseStuScore',
		),
	),
);
