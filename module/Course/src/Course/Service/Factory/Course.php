<?php

namespace Course\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Course\Db\CourseService;

class Course implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$course_service = new CourseService($db_adapter);
		return $course_service;
	}
}

?>