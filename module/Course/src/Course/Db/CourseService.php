<?php

namespace Course\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Course\Model\Course as CourseModel;
use Course\Model\CourseRole as CourseRoleModel;
use Course\Util\ServiceUtil;

class CourseService
{
	private $db_adapter;
	private $course_model;
	private $course_role_model;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->course_model = new CourseModel($this->db_adapter);
		$this->course_role_model = new CourseRoleModel($this->db_adapter);
	}
	
	public function query_by_semester($semester)
	{
		$course_result = $this->course_model->select(array('semester' => $semester));
		$course_list = array();
		foreach ($course_result as $course)
			array_push($course_list, $course);
		return $course_list;
	}
	
	/**
	 * This function differs from query_by_semester, this only query id & name for it is used for ajax
	 * @param unknown $semester
	 * @return multitype:
	 */
	public function query_course_list_by_semester($semester)
	{
		$course_result = $this->db_adapter->query(
			'SELECT id, name, subname FROM course WHERE semester = ?',
			array($semester)
		);
		$course_list = array();
		foreach ($course_result as $course)
			array_push($course_list, $course);
		return $course_list;
	}
	
	public function query_by_id($id)
	{
		$course_result = $this->course_model->select(array('id' => $id));
		return $course_result->current();
	}
	
	public function insert ($data)
	{
		$format = array(
			'name' => array('type' => ServiceUtil::RAW),
			'subname' => array('type' => ServiceUtil::RAW),
			'course_no' => array('type' => ServiceUtil::RAW),
			'semester' => array('type' => ServiceUtil::RAW),
		);
		
		$update_data = ServiceUtil::generate_update_data($format, $data);
		
		$this->course_model->insert($update_data);
		
		return array('result' => true, 'id' => $this->course_model->getLastInsertValue());
	}
	
	public function update ($id, $data)
	{
		$format = array(
			'name' => array('type' => ServiceUtil::RAW),
			'subname' => array('type' => ServiceUtil::RAW),
			'course_no' => array('type' => ServiceUtil::RAW),
		);
		$update_data = ServiceUtil::generate_update_data($format, $data);
		
		$this->course_model->update($update_data, array('id' => $id));
		
		return array('result' => true);
	}
	
	public function query_stu_list($course_id)
	{
		return $this->course_role_model->query_stu_list($course_id);
	}
	
	public function query_ta_list($course_id)
	{
		return $this->course_role_model->query_ta_list($course_id);
	}
	
	public function query_teacher_list($course_id)
	{
		return $this->course_role_model->query_teacher_list($course_id);
	}
	
	public function set_role($course_id, $role, $user_id)
	{
		switch ($role)
		{
			case 'teacher':
				$this->course_role_model->set_teacher($course_id, $user_id);
				break;
			case 'ta':
				$this->course_role_model->set_ta($course_id, $user_id);
				break;
			case 'stu':
				$this->course_role_model->set_stu($course_id, $user_id);
				break;
		}
		return array('result' => true);
	}
	
	public function remove_role($course_id, $role, $user_id)
	{
		switch ($role)
		{
			case 'teacher':
				$this->course_role_model->remove_teacher($course_id, $user_id);
				break;
			case 'ta':
				$this->course_role_model->remove_ta($course_id, $user_id);
				break;
			case 'stu':
				$this->course_role_model->remove_stu($course_id, $user_id);
				break;
		}
		return array('result' => true);
	}
}

?>