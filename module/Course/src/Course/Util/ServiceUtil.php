<?php

namespace Course\Util;

use Zend\Json\Json;

class ServiceUtil 
{
	const RAW = 0;
	const OBJ = 1;
	const ARR = 2;
	const PUSH_FRONT = 3;
	
	/**
	 * Format - Data
	 * $format = array(
			'email' => array('type' => ServiceUtil::RAW),
			'mobile' => array('type' => ServiceUtil::RAW),
			'data' => array(
				'type' => ServiceUtil::OBJ,
				'obj' => array(
					'email' => array('type' => ServiceUtil::RAW),
					'mobile' => array('type' => ServiceUtil::RAW),
					'bbsid' => array('type' => ServiceUtil::RAW),
					'icon' => array('type' => ServiceUtil::RAW),
					'test' => array('type' => ServiceUtil::ARR, 
						'obj' => array(
							'type' => ServiceUtil::RAW, 
						),
					),
					'test2' => array('type' => ServiceUtil::ARR, 
						'obj' => array(
							'type' => ServiceUtil::OBJ,
							'obj' => array(
								'arr1' => array('type' => ServiceUtil::RAW),
								'arr2' => array('type' => ServiceUtil::RAW),
							) 
						),
					),
				),
			),
		);
		$data = array(
			'email' => $data['email'],
			'mobile' => $data['mobile'],
			'bbsid' => $data['bbsid'],
			'test' => array('a', 'b'),
			'test2' => array(array('par1' => 'a', 'par2' => 'b'), array('par1' => 'c', 'par2' => 'd')),
		);
	 */
	public static function generate_update_data($format, $data, $ori_data = array())
	{
		$update_data = array();
		foreach ($format as $key => $value)
		{
			if (isset($data[$key]))
			{
				if ($value['type'] === self::OBJ)
				{
					if (isset($ori_data[$key]))
						$ori_json_value = Json::decode($ori_data[$key], true);
					else 
						$ori_json_value = array();
					$update_data[$key] = Json::encode(self::__generate_update_data($value['obj'], $data[$key], $ori_json_value));
				}
				else if ($value['type'] === self::RAW)
					$update_data[$key] = $data[$key];
				else if ($value['type'] === self::PUSH_FRONT)
					$update_data[$value['name']] = $data[$key] + $update_data[$value['name']];
				else 
					echo 'First level format has arr type!';
			}
		}
		return $update_data;
	}
	
	private static function __generate_update_data($format, $data, $update_data = array())
	{
		foreach ($format as $key => $value)
		{
			if (isset($data[$key]))
			{
				if ($value['type'] === self::OBJ)
					$update_data[$key] = self::__generate_update_data($value['obj'], $data[$key], $update_data[$key]);
				else if ($value['type'] === self::ARR)
					$update_data[$key] = self::__generate_update_data_arr($value['obj'], $data[$key]);
				else if ($value['type'] === self::RAW)
					$update_data[$key] = $data[$key];
				else if ($value['type'] === self::PUSH_FRONT)
				{
					if (isset($update_data[$value['name']]))
						$update_data[$value['name']] = $data[$key] . $update_data[$value['name']];
					else
						$update_data[$value['name']] = $data[$key];
				}
			}
		}
		return $update_data;
	}
	
	private static function __generate_update_data_arr($format, $data)
	{
		$update_data = array();
		foreach ($data as $item)
		{
			if ($format['type'] === self::OBJ)
				array_push($update_data, self::__generate_update_data($format['obj'], $item));
			else if ($format['type'] === self::ARR)
				array_push($update_data, self::__generate_update_data_arr($format['obj'], $item));
			else if ($format['type'] === self::RAW)
				array_push($update_data, $item);
		}
		return $update_data;
	}
}

?>
