<?php

namespace Course\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class Course extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course";
		$this->adapter = $adapter;
	}
}