<?php

namespace Course\Model;

use Zend\Db\Adapter\Adapter;

class CourseRole
{
	public function __construct(Adapter $adapter)
	{
		$this->adapter = $adapter;
	}
	
	public function query_ta_list($course_id)
	{
		$tempResult = $this->adapter->query('SELECT u.id AS user_id, u.name, u.email, u.mobile, u.data FROM course_user_ta AS r LEFT JOIN user AS u ON u.id = r.user_id WHERE course_id = ?', array($course_id));
		$list = array();
		foreach ($tempResult as $line)
		{
			array_push($list,
				array(
					'user_id' => $line['user_id'],
					'name' => $line['name'],
					'email' => $line['email'],
					'mobile' => $line['mobile'],
					'data' => $line['data']
				)
			);
		}
		return $list;
	}
	
	public function query_teacher_list($course_id)
	{
		$tempResult = $this->adapter->query('SELECT u.id AS user_id, u.name, u.email, u.mobile, u.data FROM course_user_teacher AS r LEFT JOIN user AS u ON u.id = r.user_id WHERE course_id = ?', array($course_id));
		$list = array();
		foreach ($tempResult as $line)
		{
			array_push($list,
				array(
					'user_id' => $line['user_id'],
					'name' => $line['name'],
					'email' => $line['email'],
					'mobile' => $line['mobile'],
					'data' => $line['data']
				)
			);
		}
		return $list;
	}
	
	public function query_stu_list($course_id)
	{
		$tempResult = $this->adapter->query('SELECT u.id AS user_id, u.name, u.email, u.mobile, u.data FROM course_user_stu AS r LEFT JOIN user AS u ON u.id = r.user_id WHERE course_id = ?', array($course_id));
		$list = array();
		foreach ($tempResult as $line)
		{
			array_push($list,
				array(
					'user_id' => $line['user_id'],
					'name' => $line['name'],
					'email' => $line['email'],
					'mobile' => $line['mobile'],
					'data' => $line['data']
				)
			);
		}
		return $list;
	}
	
	public function set_teacher($course_id, $user_id)
	{
		$this->adapter->query(
				"INSERT INTO course_user_teacher (course_id, user_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE user_id = VALUES(user_id)",
				array($course_id, $user_id)
		);
	}
	
	public function set_ta($course_id, $user_id)
	{
		$this->adapter->query(
				"INSERT INTO course_user_ta (course_id, user_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE user_id = VALUES(user_id)",
				array($course_id, $user_id)
		);
	}
	
	public function set_stu($course_id, $user_id)
	{
		$this->adapter->query(
				"INSERT INTO course_user_stu (course_id, user_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE user_id = VALUES(user_id)",
				array($course_id, $user_id)
		);
	}
	
	public function remove_teacher($course_id, $user_id)
	{
		$this->adapter->query(
				"DELETE FROM course_user_teacher WHERE course_id = ? AND user_id = ?",
				array($course_id, $user_id)
		);
	}
	
	public function remove_ta($course_id, $user_id)
	{
		$this->adapter->query(
				"DELETE FROM course_user_ta WHERE course_id = ? AND user_id = ?",
				array($course_id, $user_id)
		);
	}
	
	public function remove_stu($course_id, $user_id)
	{
		$this->adapter->query(
				"DELETE FROM course_user_stu WHERE course_id = ? AND user_id = ?",
				array($course_id, $user_id)
		);
	}
}