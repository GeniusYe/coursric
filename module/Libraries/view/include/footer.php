<footer>
	<p>
		<a href="http://www.software.fudan.edu.cn" target="__blank">
			Software School Fudan University
		</a>.
		<a id="wechat_icon">
			Wechat
		</a>
		<img class="hidden" src="<?php echo $this->basepath() . '/img/code.jpg'; ?>" />
	</p>
	<p>
		<a href="http://www.coursric.org" target="__blank">
			Coursric Production Site
		</a>. 
		<a href="http://www.coursric.org/pp" target="__blank">
			Privacy Policy
		</a>.
		<a href="<?php echo $this->url('application/default', array('controller' => 'index', 'action' => 'author')); ?>">
			About The Site
		</a>.
	</p>
	<p>
		Powered By :
		<a href="http://www.centos.org/" target="__blank">Centos</a>&nbsp;
		<a href="http://www.zend.com/en/products/server/" target="__blank">Zend Server</a>&nbsp;
		<a href="http://www.php.net/" target="__blank">PHP</a>&nbsp;
		<a href="http://dev.mysql.com/" target="__blank">MySQL Community</a>&nbsp;
		<a href="http://framework.zend.com/" target="__blank">Zend Framework</a>&nbsp;
		<a href="http://jquery.com/" target="__blank">jQuery</a>&nbsp;
		<a href="http://getbootstrap.com" target="__blank">Bootstrap</a>&nbsp;
		<a href="http://plugins.jquery.com/form/" target="__blank">jQuery Form</a>&nbsp;
	</p>
</footer>