<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/Weixin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Weixin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceLocatorInterface;
use Weixin\Model\Course as CourseModel;
use Weixin\Model\CourseStuScore as CourseStuScoreModel;
use Weixin\Util\ControllerUtil;
use Weixin\Util\ErrorType;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		define("TOKEN", "egphesflehg43wjoew");
		
		if ($this->checkSignature() !== true)
			return;
		
		ob_start();
		$this->responseMsg();
		$str = ob_get_contents();
		ob_end_clean();
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('weixin/index/index');
		$viewModel->setVariables(array('str' => $str));
		$viewModel->setTerminal(true);
		return $viewModel;
	}

	private function valid()
	{
		$echoStr = $_GET["echostr"];

		//valid signature , option
		if($this->checkSignature()){
			echo $echoStr;
			exit;
		}
	}

	private function responseMsg()
	{
		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

	  	//extract post data
		if (!empty($postStr)){
				
			  	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
				$fromUsername = $postObj->FromUserName;
				$toUsername = $postObj->ToUserName;
				$keyword = trim($postObj->Content);
				$time = time();
				$textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
							
				$linkTpl = "<xml>
							 <ToUserName><![CDATA[%s]]></ToUserName>
							 <FromUserName><![CDATA[%s]]></FromUserName>
							 <CreateTime>%s</CreateTime>
							 <MsgType><![CDATA[news]]></MsgType>
							 <ArticleCount>1</ArticleCount>
							 <Articles>
							 <item>
							 <Title><![CDATA[%s]]></Title> 
							 <Description><![CDATA[%s]]></Description>
							 <PicUrl><![CDATA[%s]]></PicUrl>
							 <Url><![CDATA[%s]]></Url>
							 </item>
							 </Articles>
							 </xml>";
				
				if(!empty( $keyword ))
				{
					if ($keyword == "bd")
					{
						$titleStr = "Please follow the following link to bind";
						$descriptionStr = "";
						$url = "http://www.etcss.fudan.edu.cn/weixin/account/bind/" . $fromUsername;
						$resultStr = sprintf($linkTpl, $fromUsername, $toUsername, $time, $titleStr, $descriptionStr, "", $url);
						//error_log($resultStr);
					}
// 					else if ($keyword == "unbd")
// 					{
// 						$url = "http://ics.fudan.edu.cn/score/wx/unbindWx.action?wxOpenId=" . $fromUsername;
// 						$ch = curl_init();
// 						curl_setopt($ch, CURLOPT_URL, $url);
// 						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 						// post数据
// 						curl_setopt($ch, CURLOPT_POST, 1);
// 						// post的变量
// 						curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
// 						$output = curl_exec($ch);
// 						curl_close($ch);
						
// 						$msgType = "text";
// 						$contentStr = "解绑成功";
// 						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
// 					}
					else if ($keyword == "cxcj")
					{
						$db_adapter = $this->getServiceLocator()->get('database_adapter');
						$course_model = new CourseModel($db_adapter);
						$courses = $course_model->select(array());

						$msgType = "text";
						$contentStr =
							"So far, we support these courses: (Name - No)\n";
						foreach ($courses as $course)
						{					
							$contentStr = $contentStr . sprintf("%s-%d\n",
									htmlspecialchars($course['name']), $course['id']);
						}
						$contentStr = $contentStr . 
							"Input 'cxcj+blank+course number' to get your score, for example 'cxcj 13'\n";
						
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					else if (substr($keyword, 0, 4) == "cxcj")
					{
						$courseId = intval(substr($keyword, 4));
						
						$msgType = "text";
						if ($courseId != 0)
						{
							$scores_result = $this->query_score($this->getServiceLocator(), $fromUsername, $courseId);
							if ($scores_result['result'] === true)
							{
								$contentStr = "Your score is listed below:\n";
								foreach ($scores_result['scores'] as $score)
									$contentStr = $contentStr . sprintf("%s : %d (%d-%d)\n", htmlspecialchars($score['abstract']), htmlspecialchars($score['value'] / 10),  htmlspecialchars($score['min_score'] / 10), htmlspecialchars($score['max_score'] / 10));
							}
							else
								$contentStr = $scores_result['reason'][0];
						}
						else
							$contentStr = "Please check your input, cxcj+blank+course number, for example 'cxcj 13'\n";
							
						error_log($contentStr);
							
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					else
					{
						$msgType = "text";
						$contentStr = 
						"Introduction：\n".
						"Type 'bd' to bind\nType 'cxcj' to get your score\n".
						"More functions are under establising.\n";
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					echo $resultStr;
				}else{
					echo "Input something...";
				}

		}else {
			echo "";
			exit;
		}
	}
	
	private function query_score(ServiceLocatorInterface $serviceLocator, $fromUsername, $course_id)
	{
		if ($course_id === null)
			return array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN));
		
		$db_adapter = $serviceLocator->get('database_adapter');
			
		/* Start Query */
		$course_model = new CourseModel($db_adapter);
		$course_result = $course_model->select(array('id' => $course_id));
		$course = $course_result->current();
		if ($course === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST));

		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($serviceLocator, $fromUsername, $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return array('result' => false, 'reason' => array(Errortype::NOT_AUTHENTICATED));
		if ($auth_status['stu'] !== true)
			return array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS));
		
		$user_id = $auth_status['user']['id'];

		/* ActualQuery */
		
		$course_stu_score_model = new CourseStuScoreModel($db_adapter);
		$scores = $course_stu_score_model->queryStuScoreByCourse($user_id, $course_id);

		return array('result' => true, 'scores' => $scores);
	}
		
	private function checkSignature()
	{
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];	
				
		$token = TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}
}
