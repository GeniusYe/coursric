<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/Weixin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Weixin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Weixin\Model\User as UserModel;
use Weixin\Model\UserCredential as UserCredentialModel;
use Weixin\Util\ControllerUtil;
use Weixin\Util\ErrorType;

class AccountController extends AbstractActionController
{
	public function bindAction()
	{
		$wx_open_id = $this->params('id');
		
		$this->layout()->setTemplate('weixin/layout/layout')->setVariables(array('navbar_active' => ''));
		
		$username = $this->params()->fromPost('username');
		$password = $this->params()->fromPost('password');
		
		if ($username !== null)
		{
			if (!is_numeric($username))
				return array('error' => '');
	
			$db_adapter = $this->serviceLocator->get('database_adapter');
			$db_adapter->getDriver()->getConnection()->beginTransaction();
			
			/* Start Query */

			$user_model = new UserModel($db_adapter);
			$user_credential_model = new UserCredentialModel($db_adapter);
			$user_result = $user_model->select(array('id' => $username));
			$user = $user_result->current();
			if ($user == null)
				return array('error' => '');
			$user_credential_result = $user_credential_model->select(array('user_id' => $username));
			$user_credential = $user_credential_result->current();
			if ($user_credential == null)
				return array('error' => '');
			
			if ($user_credential['password'] !== null)
			{
				if ($password !== $user_credential['password'])
					return array('error' => '');
			}
			else
				ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NEED_INITIALIZE_PASSWORD_FIRST)));
		
			/* Check Multiple WxId & Update */
			
			$old_wx_user_result = $user_model->select(array('wx_open_id' => $wx_open_id));
			$old_wx_user = $old_wx_user_result->current();
			if ($old_wx_user !== false && $old_wx_user['id'] !== $user['id'])
				$user_model->update(array('wx_open_id' => null), array('id' => $old_wx_user['id']));

			$user_model->update(array('wx_open_id' => $wx_open_id), array('id' => $user['id']));
			
			$db_adapter->getDriver()->getConnection()->commit();
			
			/* Rendering */

			return array('result' => true);
		}
		else
		{
			return array('wx_open_id' => $wx_open_id);
		}
	}
}
