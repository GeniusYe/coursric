<?php
namespace Weixin\Util;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Weixin\Model\User as UserModel;

class ControllerUtil
{
	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('weixin/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	public static function checkAuthentication(ServiceLocatorInterface $serviceLocator, $fromUsername, $course_id = null)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$user_model = new UserModel($db_adapter);
		
		$auth_status = array();
		
		$user_result = $user_model->select(array('wx_open_id' => $fromUsername));
		$user = $user_result->current();
		if ($user === false)
		{
			$auth_status['is_authenticated'] = false;
			return $auth_status;
		}
		else 
			$auth_status['is_authenticated'] = true;
		
		$user_id = $user['id'];
		$stu_course_list = $user_model->queryCourseAsStu($user_id);
		
		if ($course_id !== null && is_array($stu_course_list) && in_array($course_id, $stu_course_list))
			$auth_status['stu'] = true;
		else
			$auth_status['stu'] = false;
		
		return $auth_status;
	}
}