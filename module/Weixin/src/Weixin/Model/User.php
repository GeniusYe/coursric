<?php
namespace Weixin\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class User extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "user";
		$this->adapter = $adapter;
	}
	
	public function queryCourseAsTa($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_ta WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
	
	public function queryCourseAsTeacher($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_teacher WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
	
	public function queryCourseAsStu($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_stu WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
}