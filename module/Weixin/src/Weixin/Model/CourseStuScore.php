<?php

namespace Weixin\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class CourseStuScore extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course_stu_score";
		$this->adapter = $adapter;
	}
	
	/**
	 * course_id must match course_material_id, otherwise this would cause unexpected behavior
	 * @param unknown $course_id
	 * @param unknown $course_material_id
	 */
	public function queryStuScoreByCourseAndScoreItem($course_score_item_id, $course_id)
	{
		$sql = '
			SELECT u.id AS user_id, u.name AS user_name, css.value, css.rank
				FROM course_user_stu AS cus 
					JOIN user AS u ON u.id = cus.user_id
					LEFT JOIN course_stu_score AS css ON css.user_id = cus.user_id AND css.course_score_item_id = ?
				WHERE cus.course_id = ? 
				ORDER BY u.id 
				';
		return $this->adapter->query($sql, array($course_score_item_id, $course_id));
	}
	
	/**
	 * Query all the scores that one student got in one course
	 * @param unknown $user_id
	 * @param unknown $course_id
	 */
	public function queryStuScoreByCourse($user_id, $course_id)
	{
		$sql = '
			SELECT csi.abstract, csi.min_score, csi.max_score, css.value, css.rank
				FROM course_score_item AS csi
				LEFT JOIN course_stu_score AS css
					ON css.course_score_item_id = csi.id AND css.user_id = ?
				WHERE csi.course_id = ? AND csi.publicity_level = 1
				ORDER BY csi.id
			';
		return $this->adapter->query($sql, array($user_id, $course_id));
	}
	
	public function mergeScore($course_score_item_id, $user_id, $value, $rank)
	{
		$sql = '
			INSERT INTO course_stu_score (course_score_item_id, user_id, value, rank)
				VALUES (?, ?, ?, ?)
				ON DUPLICATE KEY UPDATE
				value = VALUES(value)
				';
		$this->adapter->query($sql, array($course_score_item_id, $user_id, $value, $rank));
	}
}