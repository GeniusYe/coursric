<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/User for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use User\Util\ErrorType;
use User\Util\ControllerUtil;

class AccountController extends AbstractActionController
{
	public function queryAction()
	{
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		
		$user_service = $this->getServiceLocator()->get('user_service');
		
		$user = $user_service->query_by_id($auth_status['user']['id']);
		if ($user === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::USER_DOES_NOT_EXIST)));
		
		/* Rendering */
			
		$this->layout()->setTemplate('user/layout/layout')->setVariables(array('navbar_active' => 'schedule'));
		return array(
			'user' => $user,
		);
	}
	
	public function maintainAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));

		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
				'type' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'email' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'mobile' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'bbsid' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'intro' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'icon_id' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		$user_id = $auth_status['user']['id'];
		
		$data = $this->getRequest()->getPost();
		
		$type = $filter->getValue('type');
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$user_service = $this->getServiceLocator()->get('user_service');
		
		/* Merge */

		if ($type === 'user_info')
		{
			$user_data = array(
				'email' => $filter->getValue('email'),
				'mobile' => $filter->getValue('mobile'),
				'bbsid' => $filter->getValue('bbsid'),
				'intro' => $filter->getValue('intro'),
			);
			$result = $user_service->update($user_id, array('data' => $user_data));
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		}
		else if ($type === 'subscribe_method')
		{
			if (preg_match("/^\\w+([-+.]\\w+)*@\\w+([-+.]\\w+)*\\.\\w+([-.]\\w+)*$/i", $filter->getValue('email')) !== 1)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
			$result = $user_service->update($user_id, array('email' => $filter->getValue('email'), 'mobile' => $filter->getValue('mobile')));
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		}
		else if ($type === 'icon')
		{
			$result = $user_service->update($user_id, array('data' => array('icon' => $filter->getValue('icon_id'))));
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		}
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
	   		'result' => true,
			'data' => $data,
		));
	}
}
