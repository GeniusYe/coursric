<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/User for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\View\Model\ViewModel;
use User\Util\ControllerUtil;
use User\Util\ErrorType;

class LogController extends AbstractActionController
{
	public function inAction()
	{
		$this->layout()->setTemplate('user/layout/layout')->setVariables(array('navbar_active' => ''));
		
		if (!$this->getRequest()->isPost()) 
		{	
			$frame = ControllerUtil::generateFuncaptchaFrame($this->serviceLocator);
			return array('frame' => $frame);
		}

		$username = $this->params()->fromPost('username');
		$password = $this->params()->fromPost('password');
		
		if (!is_numeric($username))
			ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$auth = $this->serviceLocator->get('auth');
		$auth_adapter = $auth->getAdapter();
		
		// below we pass the username and the password to the authentication adapter for verification
		$auth_adapter->setIdentity($username);
		$auth_adapter->setCredential($password);

		// here we do the actual verification
		$result = $auth->authenticate();
		$is_valid = $result->isValid();
		
		if(!$is_valid)
			return array('error' => $result->getCode());

		$user_service = $this->serviceLocator->get('user_service');
		
		// upon successful validation the getIdentity method returns
		// the user entity for the provided credentials
		$user = $user_service->query_by_id($username);

		$db_adapter = $this->serviceLocator->get('database_adapter');
		
		$user_service = $this->getServiceLocator()->get('user_service');
		
		$ta_course_list = $user_service->query_course_as_ta_by_user_id($user['id']);
		$teacher_course_list = $user_service->query_course_as_teacher_by_user_id($user['id']);
		$stu_course_list = $user_service->query_course_as_stu_by_user_id($user['id']);

		$auth->getStorage()->write(array('user' => $user, 'ta_course_list' => $ta_course_list, 'teacher_course_list' => $teacher_course_list, 'stu_course_list' => $stu_course_list));

		$viewModel = new ViewModel();
		$viewModel->setTemplate('user\log\login_success');
	   	$viewModel->setVariables(array('name' => $user['name']));
		
		return $viewModel;
	}
	
	public function outAction()
	{
		$auth = $this->serviceLocator->get('auth');
		$auth->clearIdentity();
		return $this->redirect()->toRoute('authentication/default', 
			array('controller' => 'log', 'action' => 'in'));
	}
	
	public function statusAction()
	{
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'course_id' => array( 'required' => false, 'validators' => array( array( 'name' => 'int' ) ) ),
		));

		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$course_id = $filter->getValue('course_id');
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), array('course_id' => $course_id));
		$data = array('result' => true);
		if ($auth_status['is_authenticated'] === true)
		{
			$data['user'] = $auth_status['user'];
			$data['ta'] = $auth_status['ta'];
			$data['teacher'] = $auth_status['teacher'];
			$data['stu'] = $auth_status['stu'];
			$data['admin'] = $auth_status['admin'];
		}
		else 
			$data['user'] = null;
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), $data);
	}
}
