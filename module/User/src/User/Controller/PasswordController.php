<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use User\Util\ErrorType;
use User\Util\ControllerUtil;
use User\Util\MailTemplate;

class PasswordController extends AbstractActionController
{
	public function resetApplyAction()
	{
		require_once(__DIR__ . '/../../../../Libraries/funcaptcha/funcaptcha.php');
		$recaptcha_config = $this->serviceLocator->get('config')['captcha'];
		$funcaptcha = new \FUNCAPTCHA();
		$verified = $funcaptcha->checkResult($recaptcha_config['private_key']);
		if (!$verified)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::RECAPTCHA_NOT_CORRECT)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'username' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->serviceLocator->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$user_service = $this->getServiceLocator()->get('user_service');
		$user_credential_service = $this->getServiceLocator()->get('user_credential_service');
		
		$user_id = $filter->getValue('username');
		
		$user = $user_service->query_by_id($user_id);
		if ($user === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::USER_DOES_NOT_EXIST)));
		
		$result = $user_credential_service->generate_reset_code($user_id);
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		else 
			$reset_code = $result['reset_code'];
		
		$db_adapter->getDriver()->getConnection()->commit();

		$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
		
		$format = MailTemplate::FORGET_PASSWORD_TEMPLATE;
		
		$body = MailTemplate::HTML_TEMPLATE .
			sprintf($format, 
				$app_name_config['domain_name'],
				$reset_code,
				$app_name_config['domain_name'],
				$reset_code,
				$app_name_config['name_display'],
				$app_name_config['domain_name']
			);
		
		$mail_service = $this->getServiceLocator()->get('mail_service');
		$mail_service->send(array(
			'subject' => 'Password Forgotten [' . $app_name_config['name_display'] . ']',
			'body' => $body,
			'to' => array(array('email' => $user['email'], 'name' => $user['name'])),
		));
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true));
	}
	
	public function resetApplySuccessAction()
	{
		$this->layout()->setTemplate('user/layout/layout')->setVariables(array('navbar_active' => ''));
		return ;
	}
	
	public function resetAction()
	{
		$this->layout()->setTemplate('user/layout/layout')->setVariables(array('navbar_active' => ''));
		return array('reset_code' => $this->params('id'));
	}
	
	public function actualResetAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$this->layout()->setTemplate('user/layout/layout')->setVariables(array('navbar_active' => ''));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'username' => array(
				'filters' => array(
					array(
						'name' => 'digits'
					)
				)
			),
			'password' => array(
				'filters' => array(
					array(
						'name' => 'stringtrim'
					)
				)
			),
			'reset_code' => array(
				'filters' => array(
					array(
						'name' => 'stringtrim'
					)
				)
			),
		));
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->serviceLocator->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$user_service = $this->getServiceLocator()->get('user_service');
		$user_credential_service = $this->getServiceLocator()->get('user_credential_service');
		
		$reset_code = $filter->getValue('reset_code');
		$password = $filter->getValue('password');
		$username = $filter->getValue('username');
		
		$user = $user_service->query_by_id($username);
		if ($user === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::USER_DOES_NOT_EXIST)));
		
		$result = $user_credential_service->reset_password_by_reset_code($username, $reset_code, $password);
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		if ($result['result'] === true)
			return array();
		else
			return ControllerUtil::generateErrorViewModel($this->getResponse(), $result);
	}
}