<?php

namespace User\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use User\Db\UserCredentialService;

class UserCredential implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$user_credential_service = new UserCredentialService($db_adapter);
		return $user_credential_service;
	}
}

?>