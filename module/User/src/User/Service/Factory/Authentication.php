<?php
namespace User\Service\Factory;

use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use User\Authentication\DbAdapter;

class Authentication implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$user_credential_service = $serviceLocator->get('user_credential_service');
		$auth_db_adapter = new DbAdapter();
		$auth_db_adapter->set_user_credential_service($user_credential_service);
		$service = new AuthenticationService();
		$service->setAdapter($auth_db_adapter);
		return $service;
	}
}