<?php

namespace User\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use User\Db\UserService;

class User implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$stored_file_service = $serviceLocator->get('stored_file_service');
		$user_service = new UserService($db_adapter, $stored_file_service);
		return $user_service;
	}
}

?>