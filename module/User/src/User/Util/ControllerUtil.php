<?php
namespace User\Util;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ControllerUtil
{
	public static function generateFuncaptchaFrame(ServiceLocatorInterface $serviceLocator)
	{
		require_once(__DIR__ . '/../../../../Libraries/funcaptcha/funcaptcha.php');
		$recaptcha_config = $serviceLocator->get('config')['captcha'];
		$funcaptcha = new \FUNCAPTCHA();
		return $funcaptcha->getFunCaptcha($recaptcha_config['public_key']);
	}

	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('user/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	
	public static function checkAuthentication(ServiceLocatorInterface $serviceLocator, $data = array())
	{
		$course_id = isset($data['course_id']) ? $data['course_id'] : null;
		
		$auth = $serviceLocator->get('auth');
		if ($auth->hasIdentity())
		{
			$auth_status = array('is_authenticated' => true);
				
			$storage = $auth->getStorage()->read();
				
			$auth_status['user'] = $storage['user'];
			
			$config = $serviceLocator->get('config');
			$admin_list = $config['admin'];

			if (in_array($auth_status['user']['id'], $admin_list))
				$auth_status['admin'] = true;
			else 
				$auth_status['admin'] = false;
			
			if ($course_id !== null && is_array($storage['ta_course_list']) && in_array($course_id, $storage['ta_course_list']))
				$auth_status['ta'] = true;
			else
				$auth_status['ta'] = false;
			if ($course_id !== null && is_array($storage['teacher_course_list']) && in_array($course_id, $storage['teacher_course_list']))
				$auth_status['teacher'] = true;
			else
				$auth_status['teacher'] = false;
			if ($course_id !== null && is_array($storage['stu_course_list']) && in_array($course_id, $storage['stu_course_list']))
				$auth_status['stu'] = true;
			else
				$auth_status['stu'] = false;
				
			return $auth_status;
		}
		else
			return array('is_authenticated' => false);
	}
}