<?php

namespace User\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Json\Json;
use RdnUpload\Service\StoredFileService;
use User\Model\User as UserModel;
use User\Util\ServiceUtil;
use User\Util\ErrorType;

class UserService
{
	private $db_adapter;
	private $user_model;
	private $stored_file_service;
	
	public function __construct(DbAdapter $adapter, StoredFileService $stored_file_service)
	{
		$this->db_adapter = $adapter;
		$this->user_model = new UserModel($this->db_adapter);
		$this->stored_file_service = $stored_file_service;
	}
	
	public function query_by_group($group)
	{
		$user_result = $this->user_model->select(array('group' => $group));
		return $user_result;
	}
	
	public function query_by_id($id)
	{
		$user_result = $this->user_model->select(array('id' => $id));
		$user = $user_result->current();
		return $user;
	}
	
	/**
	 * 
	 * @param unknown $data
	 * 		id
	 * 		name
	 * 		email
	 * 		mobile
	 * 		group
	 * 		data
	 * 			email
	 * 			mobile
	 * 			bbsid
	 * 			intro
	 * 			icon
	 */
	public function insert ($data)
	{
		$format = array(
			'id' => array('type' => ServiceUtil::RAW),
			'name' => array('type' => ServiceUtil::RAW),
			'email' => array('type' => ServiceUtil::RAW),
			'mobile' => array('type' => ServiceUtil::RAW),
			'group' => array('type' => ServiceUtil::RAW),
			'data' => array(
				'type' => ServiceUtil::OBJ,
				'obj' => array(
					'email' => array('type' => ServiceUtil::RAW),
					'mobile' => array('type' => ServiceUtil::RAW),
					'bbsid' => array('type' => ServiceUtil::RAW),
					'intro' => array('type' => ServiceUtil::RAW),
					'icon' => array('type' => ServiceUtil::RAW),
				),
			),
		);
		
		$update_data = ServiceUtil::generate_update_data($format, $data);
		
		$this->user_model->insert($update_data);
		
		return array('result' => true, 'id' => $this->user_model->getLastInsertValue());
	}
	
	/**
	 * 
	 * @param unknown $id
	 * @param unknown $data
	 * 		name
	 * 		email
	 * 		mobile
	 * 		group
	 * 		data
	 * 			email
	 * 			mobile
	 * 			bbsid
	 * 			intro
	 * 			icon
	 */
	public function update($id, $data)
	{
		$user = $this->query_by_id($id);
		if ($user === false)
			return array('result' => false, 'reason' => array(ErrorType::USER_DOES_NOT_EXIST));
		
		$format = array(
			'name' => array('type' => ServiceUtil::RAW),
			'email' => array('type' => ServiceUtil::RAW),
			'mobile' => array('type' => ServiceUtil::RAW),
			'group' => array('type' => ServiceUtil::RAW),
			'data' => array(
				'type' => ServiceUtil::OBJ,
				'obj' => array(
					'email' => array('type' => ServiceUtil::RAW),
					'mobile' => array('type' => ServiceUtil::RAW),
					'bbsid' => array('type' => ServiceUtil::RAW),
					'intro' => array('type' => ServiceUtil::RAW),
					'icon' => array('type' => ServiceUtil::RAW),
				),
			),
		);

		$update_data = ServiceUtil::generate_update_data($format, $data, $user);

		$this->user_model->update($update_data, array('id' => $id));

		/* Store Icon */
		
		if (isset($data['data']['icon']))
		{
			$user_data = Json::decode($user['data'], true);
			$this->stored_file_service->saveFile($data['data']['icon']);
			$this->stored_file_service->deleteFile($user_data['icon']);
		}
		
		return array('result' => true);
	}
	
	public function query_course_as_ta_by_user_id($user_id) 
	{
		return $this->user_model->query_course_as_ta_by_user_id($user_id);
	}
	
	public function query_course_as_teacher_by_user_id($user_id) 
	{
		return $this->user_model->query_course_as_teacher_by_user_id($user_id);
	}
	
	public function query_course_as_stu_by_user_id($user_id) 
	{
		return $this->user_model->query_course_as_stu_by_user_id($user_id);
	}
}

?>