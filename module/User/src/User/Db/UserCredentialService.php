<?php

namespace User\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use User\Model\UserCredential as UserCredentialModel;
use User\Util\ErrorType;

class UserCredentialService
{
	private $db_adapter;
	private $user_credential_model;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->user_credential_model = new UserCredentialModel($this->db_adapter);
	}
	
	public function query_by_id($id)
	{
		$user_credential_result = $this->user_credential_model->select(array('user_id' => $id));
		return $user_credential_result->current();
	}
	
	public function generate_reset_code($id)
	{
		$user_credential_result = $this->user_credential_model->select(array('user_id' => $id));
		$user_credential = $user_credential_result->current();
		if ($user_credential === false)
		{
			$this->user_credential_model->insert(array('user_id' => $id));
			$user_credential = array('id' => $id);
		}
		
		srand(time());
		$reset_code = rand(1000000000,10000000000);
		$this->user_credential_model->update(array('reset_code' => $reset_code, 'reset_time' => date('Y-m-d')), array('user_id' => $id));
		
		return array('result' => true, 'reset_code' => $reset_code);
	}
	
	public function reset_password_by_reset_code($id, $reset_code, $new_password)
	{

		$user_credential_result = $this->user_credential_model->select(array('user_id' => $id));
		$user_credential = $user_credential_result->current();
		
		if ($user_credential === false)
			return array('result' => false, 'reason' => array(ErrorType::USER_DOES_NOT_EXIST));
		
		if ($user_credential['reset_code'] === $reset_code)
		{
			$this->user_credential_model->update(array('password' => sha1(sha1('gty67u8jiohgtrfd54fy' . $new_password . 'dtrf6yuikopjhgfcrdtf')), 'reset_code' => null), array('user_id' => $id));
			return array('result' => true);
		}
		else
			return array('result' => false, 'reason' => array(ErrorType::RESET_CODE_DOES_NOT_VALID_OR_EXPIRED));
	}
}

?>