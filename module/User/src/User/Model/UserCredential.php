<?php
namespace User\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class UserCredential extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "user_credential";
		$this->adapter = $adapter;
	}
}