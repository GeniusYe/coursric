<?php
namespace User\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class User extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "user";
		$this->adapter = $adapter;
	}
	
	public function query_course_as_ta_by_user_id($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_ta WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
	
	public function query_course_as_teacher_by_user_id($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_teacher WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
	
	public function query_course_as_stu_by_user_id($user_id) 
	{
		$course_list_result = $this->adapter->query('SELECT * FROM course_user_stu WHERE user_id = ?', array($user_id));
		$course_list = array();
		foreach ($course_list_result as $line)
			array_push($course_list, $line['course_id']);
		return $course_list;
	}
}