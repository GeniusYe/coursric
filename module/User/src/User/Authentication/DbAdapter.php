<?php

namespace User\Authentication;

use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Result;
use User\Db\UserCredentialService;

class DbAdapter extends AbstractAdapter
{
	protected $user_credential_service;
	
	public function authenticate()
	{
		$identity = $this->identity;
		$password = $this->credential;
		
		$user_credential = $this->user_credential_service->query_by_id($identity);
		if ($user_credential == null)
			return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $identity);
		
		if ($user_credential['password'] !== null)
		{
			if (sha1(sha1('gty67u8jiohgtrfd54fy' . $password . 'dtrf6yuikopjhgfcrdtf')) == $user_credential['password'])
				return new Result(Result::SUCCESS, $identity);
			else
				return new Result(Result::FAILURE_CREDENTIAL_INVALID, $identity);
		}
		else
		{
			//First time use
			return new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, $identity);
		}
	}
	
	public function set_user_credential_service(UserCredentialService $user_credential_service)
	{
		$this->user_credential_service = $user_credential_service;
	}
}