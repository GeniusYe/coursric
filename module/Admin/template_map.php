<?php
// Generated by ZF2's ./bin/templatemap_generator.php
return array(
    'admin/course/query-one' => __DIR__ . '/view/admin/course/query-one.phtml',
    'admin/course/query'     => __DIR__ . '/view/admin/course/query.phtml',
    'admin/course/semester'  => __DIR__ . '/view/admin/course/semester.phtml',
    'admin/index/index'      => __DIR__ . '/view/admin/index/index.phtml',
    'admin/layout/layout'    => __DIR__ . '/view/admin/layout/layout.phtml',
    'admin/user/group'       => __DIR__ . '/view/admin/user/group.phtml',
    'admin/user/query-one'   => __DIR__ . '/view/admin/user/query-one.phtml',
    'admin/user/query'       => __DIR__ . '/view/admin/user/query.phtml',
);
