<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Admin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('navbar_active' => 'index'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('admin/index/index');
		$viewModel->setVariables(array());
		
		return $viewModel;
	}
}
