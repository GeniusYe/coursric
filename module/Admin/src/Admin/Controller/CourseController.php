<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Admin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\View\Model\ViewModel;
use Admin\Util\ControllerUtil;
use Admin\Util\ErrorType;

class CourseController extends AbstractActionController
{
	public function queryAction()
	{
		$semester = $this->params('id');
			
		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['admin'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Query */
		
		if ($semester === null)
		{
			$semester_list = $this->getServiceLocator()->get('config')['semester'];
			
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('navbar_active' => 'course'));
			
			$viewModel = new ViewModel();
			$viewModel->setTemplate('admin/course/semester');
			$viewModel->setVariables(array('semester_list' => $semester_list));
			
			return $viewModel;
		}
		
		$semester = intval($semester);
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');

		/* Rendering */

		$courses = $course_service->query_by_semester($semester);
		
		$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('navbar_active' => 'course'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('admin/course/query');
		$viewModel->setVariables(array('courses' => $courses, 'semester' => $semester));
		
		return $viewModel;
	}
	
	public function queryOneAction()
	{
		$course_id = $this->params('id');
	
		if (is_numeric($course_id))
		{
			$course_id = intval($course_id);
			
			$db_adapter = $this->getServiceLocator()->get('database_adapter');
			
			$course_service = $this->getServiceLocator()->get('course_service');
				
			/* Actual Query */
				
			$course = $course_service->query_by_id($course_id);
			if ($course === false)
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
			$semester = $course['semester'];
			
			/* Authorization Check */
	
			$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
			if ($auth_status['is_authenticated'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
			if ($auth_status['admin'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
				
			/* Rendering */
			
			$teacher_list = $course_service->query_teacher_list($course_id);
			$ta_list = $course_service->query_ta_list($course_id);
			$stu_list = $course_service->query_stu_list($course_id);
				
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('semester' => $semester, 'navbar_active' => 'course'));
			return array(
				'course' => $course,
				'semester' => $semester,
				'teacher_list' => $teacher_list,
				'ta_list' => $ta_list,
				'stu_list' => $stu_list,
			);
		}
		else
		{
			if (isset($_REQUEST['semester']))
				$semester = intval($_REQUEST['semester']);
			else
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
				
			/* Rendering */
				
			$empty = array(
				'id' => 'new',
				'name' => '',
				'subname' => '',
				'semester' => $semester,
				'course_no' => '',
			);
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('semester' => $semester, 'navbar_active' => 'course'));
			return array(
				'course' => $empty,
				'semester' => $semester,
			);
		}
	}
	
	public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'semester' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'name' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'subname' => array( 'reqired' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'course_no' => array( 'reqired' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		
		if ($id === 'new')
		{
			$semester = intval($filter->getValue('semester'));
		}
		else if (is_numeric($id))
		{
			$id = intval($id);
			$course = $course_service->query_by_id(array('id' => $id));
			if ($course === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
			$semester = $course['semester'];
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['admin'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge & Registered File Privileges */
		
		if ($id === 'new')
		{
			$result = $course_service->insert(array(
				'name' => $filter->getValue('name'),
				'subname' => $filter->getValue('subname'),
				'course_no' => $filter->getValue('course_no'),
				'semester' => $filter->getValue('semester'),
			));

			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else
				$id = $result['id'];
			
			$result = $course_info_service->insert($id, array('name' => $filter->getValue('name')));
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$course_info = $course_info_service->query_by_id($id);
			if ($course_info === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
				
			$result = $course_service->update(
				$id, array(
					'name' => $filter->getValue('name'),
					'subname' => $filter->getValue('subname'),
					'course_no' => $filter->getValue('course_no'),
					'semester' => $filter->getValue('semester'),
				)
			);
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = $course_info_service->update(
				$id, array(
					'data' => array('name' => $filter->getValue('name')),
				)
			);
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$db_adapter->getDriver()->getConnection()->commit();

		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
	
	public function queryUserAction()
	{
		$user_id = $this->params('id');
		if ($user_id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$user_service = $this->getServiceLocator()->get('user_service');

		$user = $user_service->query_by_id($user_id);
		if ($user === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::USER_DOES_NOT_EXIST)));
	
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true, 'user_id' => $user['id'], 'user_name' => $user['name']));
	}
	
	public function setRoleAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'course_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'user_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'role' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'direction' => array( 'validators' => array( array( 'name' => 'int' ) )	),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_service = $this->getServiceLocator()->get('course_service');
		
		/* Start Query */
		
		$course_id = intval($filter->getValue('course_id'));
		
		$course = $course_service->query_by_id(array('id' => $course_id));
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		
		/* Authorization Check & Merge */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		
		$role = $filter->getValue('role');
		$user_id = $filter->getValue('user_id');
		switch ($role)
		{
			case 'teacher':
				if ($auth_status['admin'] !== true)
					return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
				break;
			case 'stu':
				if ($auth_status['admin'] !== true)
					return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
				break;
			case 'ta':
				if ($auth_status['admin'] !== true && $auth_status['teacher'] !== true)
					return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
				break;
			default:
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		}
		
		$direction = intval($filter->getValue('direction'));
		
		if ($direction === 1)
		{
			$course_service->set_role($course_id, $role, $user_id);
		}
		else if ($direction === 0)
		{
			$course_service->remove_role($course_id, $role, $user_id);
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true, 'user_id' => $user_id, 'course_id' => $course_id, 'role' => $role));
	}
	
	public function setStuAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'course_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'stu_list' => array( 'filters' => array( array ( 'name' => 'stringtrim' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_service = $this->getServiceLocator()->get('course_service');
		
		/* Start Query */
		
		$course_id = intval($filter->getValue('course_id'));
		
		$course = $course_service->query_by_id(array('id' => $course_id));
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		
		/* Authorization Check & Merge */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		
		if ($auth_status['admin'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$stu_list = $course_service->query_stu_list($course_id);
		
		$stu_asso_list = array();
		foreach ($stu_list as $stu)
			$stu_asso_list[$stu['user_id']] = 0;						//0 Need Unset, 1 Need Set, 2 Normal
		unset($stu_list); 	
		
		$data = $filter->getValue('stu_list');
		$stu_line_list = explode("\n", $data);
		unset($data);
		foreach ($stu_line_list as $stu_line)
		{
			$stu_name_id_list = explode("/", $stu_line);
			if (!empty($stu_name_id_list[0]) && is_numeric($stu_name_id_list[0]))
			{
				$stu_id = $stu_name_id_list[0];
				if (isset($stu_asso_list[$stu_id]))
					$stu_asso_list[$stu_id] = 2;
				else 
					$stu_asso_list[$stu_id] = 1;
			}
			unset($stu_name_id_list);
		}
		unset($stu_line_list);
		
		foreach ($stu_asso_list as $stu_id => $state)
		{
			if ($state === 1)
				$course_service->set_role($course_id, 'stu', $stu_id);
			else if ($state === 0)
				$course_service->remove_role($course_id, 'stu', $stu_id);
		}

		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true, 'course_id' => $course_id));
	}
}
