<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Admin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\View\Model\ViewModel;
use Admin\Util\ControllerUtil;
use Admin\Util\ErrorType;

class UserController extends AbstractActionController
{
	public function queryAction()
	{
		$group = $this->params('id');
			
		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['admin'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Query */
		
		if ($group === null)
		{
			$group_list = $this->getServiceLocator()->get('config')['user_group'];
			
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('navbar_active' => 'user'));
			
			$viewModel = new ViewModel();
			$viewModel->setTemplate('admin/user/group');
			$viewModel->setVariables(array('group_list' => $group_list));
			
			return $viewModel;
		}
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$user_service = $this->getServiceLocator()->get('user_service');

		/* Rendering */

		$users = $user_service->query_by_group($group);
		
		$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('navbar_active' => 'user'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('admin/user/query');
		$viewModel->setVariables(array('users' => $users, 'group' => $group));
		
		return $viewModel;
	}
	
	public function queryOneAction()
	{
		$user_id = $this->params('id');
	
		if (is_numeric($user_id))
		{
			$db_adapter = $this->getServiceLocator()->get('database_adapter');
			
			$user_service = $this->getServiceLocator()->get('user_service');
				
			/* Actual Query */
				
			$user = $user_service->query_by_id($user_id);
			if ($user === false)
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::USER_DOES_NOT_EXIST)));
			$group = $user['group'];
			
			/* Authorization Check */
	
			$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
			if ($auth_status['is_authenticated'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
			if ($auth_status['admin'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
				
			/* Rendering */
			
			$teacher_course_list = $user_service->query_course_as_teacher_by_user_id($user_id);
			$ta_course_list = $user_service->query_course_as_ta_by_user_id($user_id);
			$stu_course_list = $user_service->query_course_as_stu_by_user_id($user_id);
				
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('group' => $group, 'navbar_active' => 'user'));
			return array(
				'user' => $user,
				'group' => $group,
				'teacher_course_list' => $teacher_course_list,
				'ta_course_list' => $ta_course_list,
				'stu_course_list' => $stu_course_list,
			);
		}
		else
		{
			if (isset($_REQUEST['group']))
				$group = intval($_REQUEST['group']);
			else
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
				
			/* Rendering */
				
			$empty = array(
				'id' => 'new',
				'name' => '',
				'group' => $group,
				'course_no' => '',
			);
			$this->layout()->setTemplate('admin/layout/layout')->setVariables(array('group' => $group, 'navbar_active' => 'user'));
			return array(
				'course' => $empty,
				'group' => $group
			);
		}
	}
	
	public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'name' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'group' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'email' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$user_service = $this->getServiceLocator()->get('user_service');
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		$user = $user_service->query_by_id(array('id' => $id));
		if ($user === false)
			$group = intval($filter->getValue('group'));
		else if (is_numeric($id))
			$group = $user['group'];
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['admin'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge & Registered File Privileges */

		if ($user === false)
		{
			$result = $user_service->insert(array(
				'id' => $filter->getValue('id'),
				'name' => $filter->getValue('name'),
				'email' => $filter->getValue('email'),
				'data' => array(
					'email' => $filter->getValue('email'),
					'mobile' => '',
					'bbsid' => '',
					'icon' => '',
				),
				'group' => $filter->getValue('group'),
			));
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else 
				$id = $result['id'];
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $user_service->update(
				$id, array(
					'name' => $filter->getValue('name'),
					'group' => $filter->getValue('group'),
					'email' => $filter->getValue('email'),
					'data' => array(
						'email' => $filter->getValue('email'),
						'mobile' => '',
						'bbsid' => '',
						'icon' => '',
					),
				)
			);
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$db_adapter->getDriver()->getConnection()->commit();

		$user = $user_service->query_by_id($id);
		
		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
}
