<?php
return array(
	'svn' => array(
		'enable' => false,
	),
	'semester' => array(
		'now' => 1,
		'detail' => array(
		),
	),
	'controllers' => array(
		'invokables' => array(
			'CourseInfo\Controller\Query' => 'CourseInfo\Controller\QueryController',
			'CourseInfo\Controller\Maintain' => 'CourseInfo\Controller\MaintainController',
			'CourseInfo\Controller\Info_Maintain' => 'CourseInfo\Controller\InfoMaintainController',
			'CourseInfo\Controller\Stu_Work' => 'CourseInfo\Controller\StuWorkController',
			'CourseInfo\Controller\Setting' => 'CourseInfo\Controller\SettingController',
		),
	),
	'router' => array(
		'routes' => array(
			'course-info' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/course_info',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'CourseInfo\Controller',
// 						'controller'	=> 'Query',
// 						'action'		=> 'schedule',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'		 => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_map' => include __DIR__  . '/../template_map.php',
		'template_path_stack' => array(
			'CourseInfo' => __DIR__ . '/../view',
		),
		'strategies' => array(
			'ViewJsonStrategy',
		),
	),
	'service_manager' => array(
		'factories' => array(
			'course_info_service' => 'CourseInfo\Service\Factory\CourseInfo',
			'course_material_service' => 'CourseInfo\Service\Factory\CourseMaterial',
			'course_stu_work_service' => 'CourseInfo\Service\Factory\CourseStuWork',
		),
	),
);
