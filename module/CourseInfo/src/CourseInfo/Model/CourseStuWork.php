<?php

namespace CourseInfo\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class CourseStuWork extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course_stu_work";
		$this->adapter = $adapter;
	}
	
	/**
	 * course_id must match course_material_id, otherwise this would cause unexpected behavior
	 * @param unknown $course_id
	 * @param unknown $course_material_id
	 */
	public function query_stu_work_by_course_and_material($course_material_id, $course_id)
	{
		$sql = '
			SELECT u.id AS user_id, u.name AS user_name, csw.data
				FROM course_user_stu AS cus 
					JOIN user AS u ON u.id = cus.user_id
					LEFT JOIN course_stu_work AS csw ON csw.user_id = cus.user_id AND csw.course_material_id = ?
				WHERE cus.course_id = ? 
				ORDER BY u.id 
				';
		return $this->adapter->query($sql, array($course_material_id, $course_id));
	}
}