<?php

namespace CourseInfo\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class CourseMaterial extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course_material";
		$this->adapter = $adapter;
	}
	
	public function query_by_id_with_work_state($user_id, $id)
	{
		$sql = '
			SELECT cm.id, cm.course_id, cm.type, cm.need_upload, cm.data, csw.data AS stu_work_data 
				FROM course_material AS cm 
				LEFT JOIN course_stu_work AS csw 
					ON csw.course_material_id = cm.id AND csw.user_id = ? 
				WHERE cm.id = ?
			';
		return $this->adapter->query($sql, array($user_id, $id));
	}
	
	public function query_by_course_id_with_work_state($user_id, $course_id)
	{
		$sql = '
			SELECT cm.id, cm.course_id, cm.type, cm.need_upload, cm.data, csw.data AS stu_work_data 
				FROM course_material AS cm 
				LEFT JOIN course_stu_work AS csw 
					ON csw.course_material_id = cm.id AND csw.user_id = ? 
				WHERE cm.course_id = ?
			';
		return $this->adapter->query($sql, array($user_id, $course_id));
	}
}