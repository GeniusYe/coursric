<?php

namespace CourseInfo\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;

class CourseInfo extends AbstractTableGateway
{
	public function __construct(Adapter $adapter)
	{
		$this->table = "course_info";
		$this->adapter = $adapter;
	}
}