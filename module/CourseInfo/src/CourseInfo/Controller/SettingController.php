<?php
namespace CourseInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Log\Db\LogService;
use CourseInfo\Util\ErrorType;
use CourseInfo\Util\ControllerUtil;
use SplFileObject;

/**
 * MaintainController
 *
 * @author GeniusYe
 * 
 */
class SettingController extends AbstractActionController
{
	public function svnPasswordAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));

		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'password' => array(),
		));
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$course_id = $this->params('id');
		
		if (!is_numeric($course_id))
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
	
		/* Start Query */
		
		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		$course_info = $course_info_service->query_by_id($course_id);
		if ($course_info === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
		
		if ($course_info['svn_status'] !== 1)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_NOT_ESTABLISHED)));
		
		/* Authorization Check */
			
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		$user_id = $auth_status['user']['id'];
		
		/* Change svn password */
		
		$password = $filter->getValue('password');
		if ($password == null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
			
		if (preg_match("/^[a-zA-Z0-9]{6,10}$/i" ,$password) !== 1)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_PASSWORD_FORMAT_ERROR)));
		$len = count($password);
		while ($len < 10)
		{
			$password = $password . ' ';
			$len++;
		}
		
		$config = $this->getServiceLocator()->get('config');
		$svn_config = $config['svn'];
		
		if ($svn_config['enable'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_NOT_ENABLED)));
		
		$file_path = $svn_config['directory'] . '/course_' . $course['id'] . '/conf/passwd';
		$file = new SplFileObject($file_path, "r+");
		$i = 0;
		while (!$file->eof())
		{
			$line = $file->fgets();
			if (strpos($line, 'user' . $user_id) === 0)
			{
				break;
			}
			$file->next();
			$i++;
		}
		if (!$file->eof())
		{
			if ($i > 0)
				$file->seek($i - 1);
			else
				$file->rewind();
			$file->fwrite('user' . $user_id . ' = ' . $password, 28);
		}
		else 
		{
			$file->fwrite('user' . $user_id . ' = ' . $password, 28);
			$file->fwrite("\r\n");
		}
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::SVN_PASSWORD_RETRIEVE, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_id));
	
		/* Rendering */

		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	public function establishSvnAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$course_id = $this->params('id');
	
		if (!is_numeric($course_id))
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');

		/* Start Query */

		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		$course_info = $course_info_service->query_by_id($course_id);
		if ($course_info === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
			
		if ($course_info['svn_status'] === 1)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_ALREADY_ESTABLISHED)));
			
		/* Authorization Check */

		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Establish Svn */

		$user_id = $auth_status['user']['id'];
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_id = $course['id'];
		$course_teacher_list = $course_service->query_teacher_list($course_id);
		$course_ta_list = $course_service->query_ta_list($course_id);
		$course_stu_list = $course_service->query_stu_list($course_id);

		$config = $this->getServiceLocator()->get('config');
		$config = $config['svn'];
		
		if ($config['enable'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_NOT_ENABLED)));
		
		$repository_name = 'course_' . $course['id'];
		$svn_directory = $config['directory'] . '/' . $repository_name;
		
		if (file_exists($svn_directory))
		{
			$course_info_service->update ($course_id, array('svn_status' => 1));
			$db_adapter->getDriver()->getConnection()->commit();
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_ALREADY_ESTABLISHED)));
		}
		
		$template_directory = rtrim($config['template_directory'], DIRECTORY_SEPARATOR);
		
		exec('"' . $config['svn_root'] . '/svnadmin" create "' . $svn_directory . '"');
		
		$this->reset_config_files(
			array(
				'template_directory' => $template_directory,
				'repository_name' => $repository_name,
				'svn_directory' => $svn_directory,
			),
			array(
				'ta_list' => $course_ta_list,
				'teacher_list' => $course_teacher_list,
				'stu_list' => $course_stu_list,
			)
		);
		
		/* Passwd */
		
		ob_start();
		
		require (rtrim($config['template_directory'], DIRECTORY_SEPARATOR) . '/passwd');
		
		file_put_contents($svn_directory . '/conf/passwd', ob_get_contents());
		ob_end_clean();
		
		/* Database Update */

		$result = $course_info_service->update ($course_id, array('svn_status' => 1));
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::SVN_ESTABLISH, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_id));
		
		/* Rendering */

		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	public function refreshSvnAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$course_id = $this->params('id');
	
		if (!is_numeric($course_id))
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
	
		/* Start Query */
	
		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		$course_info = $course_info_service->query_by_id($course_id);
		if ($course_info === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
			
		if ($course_info['svn_status'] !== 1)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_NOT_ESTABLISHED)));
			
		/* Authorization Check */
	
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
	
		/* Establish Svn */
	
		$user_id = $auth_status['user']['id'];
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_id = $course['id'];
		$course_teacher_list = $course_service->query_teacher_list($course_id);
		$course_ta_list = $course_service->query_ta_list($course_id);
		$course_stu_list = $course_service->query_stu_list($course_id);
	
		$config = $this->getServiceLocator()->get('config');
		$config = $config['svn'];
		
		if ($config['enable'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::SVN_NOT_ENABLED)));
		
		$repository_name = 'course_' . $course['id'];
		$svn_directory = $config['directory'] . '/' . $repository_name;
	
		$this->reset_config_files(
			array(
				'template_directory' => rtrim($config['template_directory'], DIRECTORY_SEPARATOR),
				'repository_name' => $repository_name,
				'svn_directory' => $svn_directory,
			),
			array(
				'ta_list' => $course_ta_list,
				'teacher_list' => $course_teacher_list,
				'stu_list' => $course_stu_list,
			)
		);
		
		/* Database Update */
		
		$result = $course_info_service->update($course_id, array('svn_status' => 1));
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::SVN_REFRESH, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_id));
	
		/* Rendering */
	
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	private function reset_config_files($svn_info, $course_roles)
	{
		/* Authz */
		
		ob_start();
		
		require ($svn_info['template_directory'] . '/authz');
		
		$ta_authz = '';
		
		echo "########## Overall ##########\r\n\r\n";
		echo '[' . $svn_info['repository_name'] . ':/]' . "\r\n";
		foreach ($course_roles['ta_list'] as $ta)
		{
			$ta_authz = $ta_authz . 'user' . $ta['user_id'] . ' = rw' . "\r\n";
		}
		echo $ta_authz;
		echo '* = ' . "\r\n";
		echo "\r\n";
		
		echo "########## TA ##########\r\n\r\n";
		foreach ($course_roles['ta_list'] as $ta)
		{
			echo '[' . $svn_info['repository_name'] . ':/user' . $ta['user_id'] . ']' . "\r\n";
			echo $ta_authz;
			echo '* = ' . "\r\n";
			echo "\r\n";
		}
		
		echo "########## Stu ##########\r\n\r\n";
		foreach ($course_roles['stu_list'] as $stu)
		{
			echo '[' . $svn_info['repository_name'] . ':/user' . $stu['user_id'] . ']' . "\r\n";
			echo $ta_authz;
			echo 'user' . $stu['user_id'] . ' = rw' . "\r\n";
			echo '* = ' . "\r\n";
			echo "\r\n";
		}
		
		file_put_contents($svn_info['svn_directory'] . '/conf/authz', ob_get_contents());
		ob_end_clean();
		
		/* svnserve.conf */
		
		ob_start();
		
		require ($svn_info['template_directory'] . '/svnserve.conf');
		
		file_put_contents($svn_info['svn_directory'] . '/conf/svnserve.conf', ob_get_contents());
		ob_end_clean();
	}
}