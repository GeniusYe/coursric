<?php

namespace CourseInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\Json\Json;
use Log\Db\LogService;
use CourseInfo\Util\ErrorType;
use CourseInfo\Util\ControllerUtil;
use CourseInfo\Util\MailTemplate;

class InfoMaintainController extends AbstractActionController
{
	public function infoAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$course_id = $this->params('id');
		
		if (is_numeric($course_id))
		{
			$db_adapter = $this->getServiceLocator()->get('database_adapter');
			$db_adapter->getDriver()->getConnection()->beginTransaction();

			$course_info_service = $this->getServiceLocator()->get('course_info_service');
			
			/* Start Query */
			
			$course_info = $course_info_service->query_by_id($course_id);
			if ($course_info === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
			
			/* Authorization Check */
			
			$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
			if ($auth_status['is_authenticated'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
			if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
			
			/* Modification */
			
			$factory = new FilterFactory();
			$filter = $factory->createInputFilter(array(
				'type' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
				'value' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			));
			
			$raw_data = $this->getRequest()->getPost();
			$filter->setData($raw_data);
			
			if (!$filter->isValid())
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
				
			$type = $filter->getValue('type');
			if ($type !== 'goals' && $type !== 'contents' && $type !== 'news' && $type !== 'add_news')
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

			$new_value = $filter->getValue('value');
			if ($type === 'add_news')
				$new_value = $new_value . "\n";
			
			/* Merge Course Info */
			
			$result = $course_info_service->update ($course_id, array('data' => array($type => $new_value)));
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$db_adapter->getDriver()->getConnection()->commit();
		
			/* Log */
			
			$log_service = $this->getServiceLocator()->get('log_service');
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::INFO_MAINTAIN, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_id, 'optional_str' => $type, 'long_info' => 'Update course info ' . $type . ' to new value : ' . $new_value));
			
			/* Notification */
			
			if ($type === 'add_news')
			{
				$course_service = $this->getServiceLocator()->get('course_service');
				
				$mail_list = ControllerUtil::get_course_mail_list($course_service, $course_id);
					
				$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
				$this->send_notification(
					array(
						'course_info' => $course_info,
						'news' => $new_value,
						'mail_list' => $mail_list,
					),
					$app_name_config
				);
		
				/* Log */
				
				$log_service = $this->getServiceLocator()->get('log_service');
				$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MESSAGE_ADD, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_id, 'optional_str' => $type, 'long_info' => 'Send course messages to students : ' . $new_value));
			}
			
			/* Rendering */
			
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true, 'type' => $type, 'value' => $new_value));
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	}
	
	private function send_notification($data, $app_name_config)
	{
		$course_info = $data['course_info'];
		$course_info_data = Json::decode($course_info['data'], true);
			
		$format = MailTemplate::INFO_TEMPLATE;
		$subject = 'New Course News Released On "' . $course_info_data['name'] . '" [' . $app_name_config['name_display'] . ']';
	
		$body = MailTemplate::HTML_TEMPLATE .
			sprintf($format,
				$course_info_data['name'],
				$data['news'],
				$app_name_config['domain_name'],
				$course_info['course_id'],
				$app_name_config['domain_name'],
				$course_info['course_id'],
				$app_name_config['name_display'],
				$app_name_config['domain_name']
			);
			
		$mail_service = $this->getServiceLocator()->get('mail_service');
		$mail_service->send(array(
			'subject' => $subject,
			'body' => $body,
			'to' => $data['mail_list']['to_list'],
			'cc' => $data['mail_list']['cc_list'],
			'attachments' => array(),
		));
	}
}