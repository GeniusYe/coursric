<?php
namespace CourseInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CourseInfo\Util\ControllerUtil;
use CourseInfo\Util\ErrorType;

/**
 * MaintainController
 *
 * @author GeniusYe
 *
 * @version
 *
 */
class QueryController extends AbstractActionController
{
	public function introductionAction()
	{
		$course_id = $this->params('id');
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		
		/* Query */
		
		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		$course_info = $course_info_service->query_by_id($course_id);
		if ($course_info === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		
		/* Rendering */

		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'introduction'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/query/course-info');
		$viewModel->setVariables(
				array(
					'course_id' => $course_id,
					'name' => $course['name'],
					'subname' => $course['subname'],
					'course_info' => $course_info,
					'auth_status' => $auth_status
				));
		
		return $viewModel;
	}

	public function facultyAction()
	{
		$course_id = $this->params('id');
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		
		$course_service = $this->getServiceLocator()->get('course_service');
		
		$ta_list = $course_service->query_ta_list($course_id);
		$teacher_list = $course_service->query_teacher_list($course_id);
		
		/* Rendering */

		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'faculty'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/query/course-faculty');
		$viewModel->setVariables(array('course_id' => $course_id, 'ta_list' => $ta_list, 'teacher_list' => $teacher_list));
		
		return $viewModel;
	}
		
	private static function cmp($a, $b)
	{
		if ($a['data']["publish_time"] > $b['data']["publish_time"])
			return 1;
		else if ($a['data']["publish_time"] < $b['data']["publish_time"])
			return -1;
		else
		{
			if ($a['id'] > $b['id'])
				return 1;
			else if ($a['id'] < $b['id'])
				return -1;
			else 
				return 0;
		}
	}
	
	public function scheduleAction()
	{
		$course_id = $this->params('id');
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		
		/* Query */ 
		
		if ($auth_status['is_authenticated'] && $auth_status['stu'] === true)
			$course_materials = $course_material_service->query_by_course_id_with_work_state($auth_status['user']['id'], $course_id);
		else
			$course_materials = $course_material_service->query_by_course_id($course_id);
		
		/* Rendering */
		
		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'schedule'));
		
		$course_materials_list = array();
		$course_materials_list[0] = array();
		$course_materials_list[1] = array();
		$course_materials_list[2] = array();
		$course_materials_list[3] = array();
		$course_materials_list[4] = array();
		$course_materials_list[5] = array();
		
		foreach ($course_materials as $material)
		{
			$offset = $material['type'];
			if (isset($material['stu_work_data']))
				array_push($course_materials_list[$offset], array('id' => $material['id'], 'data' => \Zend\Json\Json::decode($material['data'], true), 'need_upload' => $material['need_upload'], 'stu_work_data' => $material['stu_work_data']));
			else
				array_push($course_materials_list[$offset], array('id' => $material['id'], 'data' => \Zend\Json\Json::decode($material['data'], true), 'need_upload' => $material['need_upload'], 'stu_work_data' => null));
		}
		unset($course_materials);
		usort($course_materials_list[0], array('CourseInfo\Controller\QueryController', 'cmp'));
		usort($course_materials_list[1], array('CourseInfo\Controller\QueryController', 'cmp'));
		usort($course_materials_list[2], array('CourseInfo\Controller\QueryController', 'cmp'));
		usort($course_materials_list[3], array('CourseInfo\Controller\QueryController', 'cmp'));
		usort($course_materials_list[4], array('CourseInfo\Controller\QueryController', 'cmp'));
		usort($course_materials_list[5], array('CourseInfo\Controller\QueryController', 'cmp'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/query/course-material');
		$viewModel->setVariables(array('course_id' => $course_id, 'auth_status' => $auth_status));

		$viewModelT = new ViewModel();
		$viewModelT->setTemplate('course-info/query/course-material-table-lecture');
		$viewModelT->setVariables(array('materials' => $course_materials_list[0], 'auth_status' => $auth_status));
		$viewModel->addChild($viewModelT, 'MT_0');

		$viewModelT = new ViewModel();
		$viewModelT->setTemplate('course-info/query/course-material-table-video');
		$viewModelT->setVariables(array('materials' => $course_materials_list[5], 'auth_status' => $auth_status));
		$viewModel->addChild($viewModelT, 'MT_5');
		
		$this->generateTable($viewModel, $course_materials_list[1], 'MT_1', $auth_status);
		$this->generateTable($viewModel, $course_materials_list[2], 'MT_2', $auth_status);
		$this->generateTable($viewModel, $course_materials_list[3], 'MT_3', $auth_status);
		$this->generateTable($viewModel, $course_materials_list[4], 'MT_4', $auth_status);
		
		return $viewModel;
	}
	
	public function scheduleOneAction()
	{
		$course_material_id = $this->params('id');
		if ($course_material_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		
		/* Query */
		
		$course_material = $course_material_service->query_by_id($course_material_id);
		if ($course_material === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		
		$course_id = $course_material['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		
		if ($auth_status['is_authenticated'] && $auth_status['stu'] === true)
			$course_material = $course_material_service->query_by_id_with_work_state($auth_status['user']['id'], $course_material_id);
		else
			$course_material = $course_material_service->query_by_id($course_material_id);
		
		/* Rendering */
		
		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'schedule'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/query/course-material-one');
		$viewModel->setVariables(array('course_id' => $course_id ,'course_material' => $course_material, 'auth_status' => $auth_status));
		return $viewModel;
	}
	
	/**
	 * @param viewModel
	 * @param course_materials
	 * @param type
	 */
	private function generateTable($viewModel, $course_materials, $type, $auth_status) 
	{
		$viewModelT = new ViewModel();
		$viewModelT->setTemplate('course-info/query/course-material-table');
		$viewModelT->setVariables(array('materials' => $course_materials, 'auth_status' => $auth_status));
		$viewModel->addChild($viewModelT, $type);
	}
	
	public function settingAction()
	{
		$course_id = $this->params('id');
		if ($course_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');

		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		
		/* Query */
		
		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
		$course_info = $course_info_service->query_by_id($course_id);
		if ($course_info === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));

		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::NOT_AUTHENTICATED)));
		
		$svn_config = $this->getServiceLocator()->get('config')['svn'];
		
		$render_data = array('course' => $course, 'course_info' => $course_info, 'svn_config' => $svn_config, 'auth_status' => $auth_status);
		if ($auth_status['ta'] === true || $auth_status['teacher'] === true || $auth_status['admin'] === true)
			$render_data['stu_list'] = $course_service->query_stu_list($course['id']);

		/* Rendering */
		
		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'setting'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/query/course-setting');
		$viewModel->setVariables($render_data);
		
		return $viewModel;
			
	}
}
