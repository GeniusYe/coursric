<?php
namespace CourseInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\Json\Json;
use Zend\View\Model\ViewModel;
use Log\Db\LogService;
use CourseInfo\Util\ErrorType;
use CourseInfo\Util\ControllerUtil;

/**
 * MaintainController
 *
 * @author GeniusYe
 *
 */
class StuWorkController extends AbstractActionController
{

	public function uploadAction()
	{
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'course_material_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'file_id' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		$course_stu_work_service = $this->getServiceLocator()->get('course_stu_work_service');

		/* Check material to make sure work upload needed */
		
		$course_material_id = $filter->getValue('course_material_id');
		$course_material = $course_material_service->query_by_id($course_material_id);
		if ($course_material === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		if ($course_material['need_upload'] !== 1)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_NEED_WORK)));
		
		/* Authorization Check */
		
		$course_id = $course_material['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['stu'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Merge Student Work */
		
		$file_id = $filter->getValue('file_id');
		$user_id = $auth_status['user']['id'];
		
		$course_stu_work_data = array('stored_file_id' => $file_id, 'stored_file_name' => pathinfo($file_id, PATHINFO_EXTENSION), 'submitted_time' => time());
		
		$result = $course_stu_work_service->merge(array('course_material_id' => $course_material_id, 'user_id' => $user_id, 'data' => $course_stu_work_data));
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::STU_WORK_SUBMIT, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $course_material_id));
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
			
	}
	
	/**
	 * This function is used by TA only, to check all the student work by course material id
	 * @return \Zend\View\Model\ViewModel
	 */
	public function queryAction()
	{
		$course_material_id = $this->params('id');
		if ($course_material_id == null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		$course_stu_work_service = $this->getServiceLocator()->get('course_stu_work_service');
		
		/* Check material to make sure work upload needed */
		
		$course_material = $course_material_service->query_by_id($course_material_id);
		if ($course_material === false)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		
		/* Authorization Check */
		
		$course_id = $course_material['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
			
		/* Actual Query */
		
		$course_stu_work_result = $course_stu_work_service->query_stu_work_by_course_and_material($course_material_id, $course_id);

		$course_material_data = Json::decode($course_material['data'], true);
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::STU_WORK_QUERY, $auth_status['user']['id'], LogService::INFO, array('optional_id' => $course_material_id));
		
		/* Rendering */
		
		$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'stu_work'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/stu-work/query');
		$viewModel->setVariables(array('course_material_abstract' => $course_material_data['abstract'], 'course_stu_works' => $course_stu_work_result));
		return $viewModel;
	}
}