<?php
namespace CourseInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\InputFilter\Factory as FilterFactory;
use Zend\Json\Json;
use Log\Db\LogService;
use CourseInfo\Util\ErrorType;
use CourseInfo\Util\ControllerUtil;
use CourseInfo\Util\MailTemplate;

/**
 * MaintainController
 *
 * @author GeniusYe
 * 
 */
class MaintainController extends AbstractActionController
{

	public function queryOneAction()
	{
		$course_material_id = $this->params('id');
		
		if (is_numeric($course_material_id))
		{
			$db_adapter = $this->getServiceLocator()->get('database_adapter');
			$course_material_service = $this->getServiceLocator()->get('course_material_service');
			
			/* Actual Query */
			
			$course_material = $course_material_service->query_by_id($course_material_id);
			if ($course_material === false)
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		
			/* Authorization Check */
		
			$course_id = $course_material['course_id'];
			$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
			if ($auth_status['is_authenticated'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
			if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
			
			/* Rendering */
			
			$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'schedule'));
			return array(
				'course_material' => $course_material,
				'course_id' => $course_id
			);
		}
		else
		{
			if (isset($_REQUEST['course_id']))
				$course_id = $_REQUEST['course_id'];
			else
				return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
			
			/* Rendering */
			
			$empty = array(
				'id' => 'new',
				'type' => 0,
				'course_id' => $course_id,
				'need_upload' => 0,
				'data' => Json::encode(array(
					'abstract' => '',
					'publish_time' => time(),
					'deadlines' => array(),
					'instructors' => array(),
					'attachments' => array()
				))
			);
			$this->layout()->setTemplate('course-info/layout/layout')->setVariables(array('course_id' => $course_id, 'navbar_active' => 'schedule'));
			return array(
				'course_material' => $empty,
				'course_id' => $course_id
			);
		}
	}

	public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'course_id' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'type' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'abstract' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'publish_time' => array( 'required' => false ),
			'need_upload' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'deadline' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'instructor' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'attachment' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'abstract_md' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$instructors = array();
		$deadline = array();
		$attachment = array();

		if (!empty($filter->getValue('deadline')))
		{
			$temp = explode("\n", $filter->getValue('deadline'));
			foreach ($temp as $t)
				if (trim($t) !== "")
					array_push($deadline, strtotime($t));
		}
		if (!empty($filter->getValue('instructor')))
		{
			$temp = explode("\n", $filter->getValue('instructor'));
			foreach ($temp as $t)
				if (trim($t) !== "")
					array_push($instructors, $t);
		}
		if (!empty($filter->getValue('attachment')))
		{
			$temp = explode("\n", $filter->getValue('attachment'));
			foreach ($temp as $t)
				if (trim($t) !== "")
				{
					$temp2 = explode(",", $t);
					if (is_array($temp2) && count($temp2) === 2)
						array_push($attachment, array('name' => $temp2[0], 'id' => $temp2[1]));
				}
		}
			
		$data = array(
			'abstract' => $filter->getValue('abstract'),
			'abstract_md' => $filter->getValue('abstract_md'),
			'publish_time' => strtotime($filter->getValue('publish_time')),
			'deadlines' => $deadline,
			'instructors' => $instructors,
			'attachments' => $attachment
		);
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		$course_service = $this->getServiceLocator()->get('course_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		
		if ($id === 'new')
		{
			$course_id = $filter->getValue('course_id');
			$type = $filter->getValue('type');
		}
		else if (is_numeric($id))
		{
			$course_material = $course_material_service->query_by_id(array('id' => $id));
			if ($course_material === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
			$course_id = $course_material['course_id'];
			$type = $course_material['type'];
		}
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		$course = $course_service->query_by_id($course_id);
		if ($course === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_DOES_NOT_EXIST)));
	
		/* Authorization Check */
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge & Registered File Privileges */
		
		if ($id === 'new')
		{
			$result = $course_material_service->insert(array(
				'course_id' => $filter->getValue('course_id'),
				'type' => $filter->getValue('type'),
				'need_upload' => $filter->getValue('need_upload'),
				'data' => $data,
			));
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else 
				$id = $result['id'];
			
			$notification_operation = 'add';
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $course_material_service->update(
				$id, array(
					'data' => $data,
					'need_upload' => $filter->getValue('need_upload'),
				)
			);
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$notification_operation = 'edit';
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$course_info_service->update ($course_id, array('last_schedule_modify' => date('Y-m-d', time())));
	
		$db_adapter->getDriver()->getConnection()->commit();

		$course_material = $course_material_service->query_by_id($id);
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		if ($notification_operation === 'add')
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MATERIAL_ADD, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Create course material with name ' . $data['abstract'] . ' under course ' . $course['name'] . '(' . $course['id'] . ')'));
		else if ($notification_operation === 'edit')
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MATERIAL_EDIT, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Update course material with new name ' . $data['abstract']));
		
		/* Mail Notification */
		
		if ($this->params()->fromPost('if_notification'))
		{
			$course_info = $course_info_service->query_by_id($course_id);
		
			$mail_list = ControllerUtil::get_course_mail_list($course_service, $course_id);
			
			$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
			$this->send_notification(
				array(
					'course_info' => $course_info,
					'course_material' => $course_material,
					'course_material_data' => $data,
					'operation' => $notification_operation,
					'mail_list' => $mail_list,
				),
				$app_name_config
			);
			
			/* Log */
			
			$log_service = $this->getServiceLocator()->get('log_service');
			$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MATERIAL_NOTIFY, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Notify students of course material with name ' . $data['abstract']));
		}
		
		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
	
	public function deleteAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
	
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_stu_work_service = $this->getServiceLocator()->get('course_stu_work_service');
		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		
		/* Start Query */

		$course_material = $course_material_service->query_by_id($id);
		if ($course_material === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		
		/* Authorization Check */
		
		$course_id = $course_material['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		/* Merge */
		
		$course_stu_work_service->delete_by_course_material_id($id);

		$result = $course_material_service->delete(array("id" => $id));
		if ($result['result'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
		
		$result = true;
		
		$db_adapter->getDriver()->getConnection()->commit();
		
		/* Log */
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MATERIAL_DELETE, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id));
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
	
	public function notifyAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$id = $this->params('id');
		
		if ($id === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
				
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$course_material_service = $this->getServiceLocator()->get('course_material_service');
		$course_info_service = $this->getServiceLocator()->get('course_info_service');
		
		/* Start Query */

		$course_material = $course_material_service->query_by_id($id);
		if ($course_material === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST)));
		
		$course_info = $course_info_service->query_by_id($course_material['course_id']);
		if ($course_info === false)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST)));
		
		/* Authorization Check */
		
		$course_id = $course_material['course_id'];
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator(), $course_id);
		if ($auth_status['is_authenticated'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if ($auth_status['ta'] !== true && $auth_status['teacher'] !== true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		/* Log */
		
		$course_material_data = Json::decode($course_material['data'], true);
		
		$log_service = $this->getServiceLocator()->get('log_service');
		$log_service->log(ControllerUtil::MODULE_ID, ControllerUtil::MATERIAL_NOTIFY, $auth_status['user']['id'], LogService::SENSITIVE, array('optional_id' => $id, 'short_info' => 'Notify students of course material with name ' . $course_material_data['abstract']));
		
		/* Mail Notification */

		$course_service = $this->getServiceLocator()->get('course_service');
		
		$mail_list = ControllerUtil::get_course_mail_list($course_service, $course_id);
			
		$app_name_config = $this->getServiceLocator()->get('config')['app_name'];
		$this->send_notification(
			array(
				'course_info' => $course_info,
				'course_material' => $course_material,
				'course_material_data' => $course_material_data,
				'operation' => 'notify',
				'mail_list' => $mail_list,
			),
			$app_name_config
		);
		
		/* Rendering */
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => true
		));
	}
	
	private function send_notification($data, $app_name_config)
	{
		$course_info = $data['course_info'];
		$course_info_data = Json::decode($course_info['data'], true);
		$course_material = $data['course_material'];
		$course_material_data = $data['course_material_data'];

		if ($data['operation'] === 'add')
		{
			$format = MailTemplate::MATERIAL_CREATE_TEMPLATE;
			$subject = 'New Course Material Released On "' . $course_info_data['name'] . '" [' . $app_name_config['name_display'] . ']';
		}
		else if ($data['operation'] === 'notify' || $data['operation'] === 'edit')
		{
			$format = MailTemplate::MATERIAL_EDIT_TEMPLATE;
			$subject = 'Course Material Notification On "' . $course_info_data['name'] . '" [' . $app_name_config['name_display'] . ']';
		}
		
		$body = MailTemplate::HTML_TEMPLATE . 
			sprintf($format, 
				$course_info_data['name'],
				$course_material_data['abstract'],
				$app_name_config['domain_name'],
				$course_material['course_id'],
				$app_name_config['domain_name'],
				$course_material['course_id'],
				$app_name_config['name_display'],
				$app_name_config['domain_name']
			);
					
		$attachments_arr = $course_material_data['attachments'];
		if (isset($course_material_data['abstract_md']) && $course_material_data['abstract_md'] != '')
		{
			array_push($attachments_arr, array('id' => $course_material_data['abstract_md'], 'name' => 'abstract.md'));
		}
		$mail_service = $this->getServiceLocator()->get('mail_service');
		$mail_service->send(array(
			'subject' => $subject,
			'body' => $body,
			'to' => $data['mail_list']['to_list'],
			'cc' => $data['mail_list']['cc_list'],
			'attachments' => $attachments_arr,
		));
	}
}