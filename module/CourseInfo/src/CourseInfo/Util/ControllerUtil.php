<?php
namespace CourseInfo\Util;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Course\Db\CourseService;

class ControllerUtil
{
	const MODULE_ID = 1;
	
	const MATERIAL_ADD = 1;
	const MATERIAL_EDIT = 2;
	const MATERIAL_DELETE = 3;
	const MATERIAL_NOTIFY = 4;
	const INFO_MAINTAIN = 10;
	const MESSAGE_ADD = 11;
	const STU_WORK_SUBMIT = 20;
	const STU_WORK_QUERY = 21;
	const SVN_ESTABLISH = 30;
	const SVN_REFRESH = 31;
	const SVN_PASSWORD_RETRIEVE = 32;
	
	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	public static function checkAuthentication(ServiceLocatorInterface $serviceLocator, $course_id = null)
	{
		$auth = $serviceLocator->get('auth');
		if ($auth->hasIdentity())
		{
			$auth_status = array('is_authenticated' => true);
				
			$storage = $auth->getStorage()->read();
				
			$auth_status['user'] = $storage['user'];
			
			$config = $serviceLocator->get('config');
			$admin_list = $config['admin'];

			if (in_array($auth_status['user']['id'], $admin_list))
				$auth_status['admin'] = true;
			else 
				$auth_status['admin'] = false;
			
			if ($course_id !== null && is_array($storage['ta_course_list']) && in_array($course_id, $storage['ta_course_list']))
				$auth_status['ta'] = true;
			else
				$auth_status['ta'] = false;
			if ($course_id !== null && is_array($storage['teacher_course_list']) && in_array($course_id, $storage['teacher_course_list']))
				$auth_status['teacher'] = true;
			else
				$auth_status['teacher'] = false;
			if ($course_id !== null && is_array($storage['stu_course_list']) && in_array($course_id, $storage['stu_course_list']))
				$auth_status['stu'] = true;
			else
				$auth_status['stu'] = false;
				
			return $auth_status;
		}
		else
			return array('is_authenticated' => false);
	}
	
	public static function get_course_mail_list(CourseService $course_service, $course_id)
	{
		$mail_list = array();
		$stuff_list = $course_service->query_stu_list($course_id);
		foreach ($stuff_list as $stuff)
			if ($stuff['email'] != null) 
				array_push($mail_list, array('email' => $stuff['email'], 'name' => $stuff['name']));
		
		$cc_mail_list = array();
		$stuff_list = $course_service->query_ta_list($course_id);
		foreach ($stuff_list as $stuff)
			if ($stuff['email'] != null) 
				array_push($cc_mail_list, array('email' => $stuff['email'], 'name' => $stuff['name']));
		$stuff_list = $course_service->query_teacher_list($course_id);
		foreach ($stuff_list as $stuff)
			if ($stuff['email'] != null) 
				array_push($cc_mail_list, array('email' => $stuff['email'], 'name' => $stuff['name']));
			
		return array('to_list' => $mail_list, 'cc_list' => $cc_mail_list);
	}
}