<?php

namespace CourseInfo\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Json\Json;
use RdnUpload\Service\StoredFileService;
use CourseInfo\Model\CourseStuWork as CourseStuWorkModel;
use CourseInfo\Util\ErrorType;

class CourseStuWorkService
{
	private $db_adapter;
	private $course_material_service;
	private $course_stu_work_model;
	
	public function __construct(DbAdapter $adapter, CourseMaterialService $course_material_service, StoredFileService $stored_file_service)
	{
		$this->db_adapter = $adapter;
		$this->course_material_service = $course_material_service;
		$this->course_stu_work_model = new CourseStuWorkModel($this->db_adapter);
		$this->stored_file_service = $stored_file_service;
	}

	public function query_by_user_course_material($user_id, $course_material_id)
	{
		$course_stu_work_result = $this->course_stu_work_model->select(array('user_id' => $user_id, 'course_material_id' => $course_material_id));
		return $course_stu_work_result->current();
	}

	/**
	 * @param $data:
	 * 		user_id
	 * 		course_material_id
	 * 		course_stu_work_data
	 * 			stored_file_id
	 * 			stored_file_name
	 * 			submitted_time
	 *
	 * @return multitype:boolean multitype:unknown  |number
	 */
	public function merge($data)
	{
		$user_id = $data['user_id'];
		
		$course_material = $this->course_material_service->query_by_id($data['course_material_id']);
		if ($course_material === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST));
		
		$course_id = $course_material['course_id'];
		
		/* Merge */
		
		$course_stu_work = $this->query_by_user_course_material($user_id, $data['course_material_id']);
		if ($course_stu_work === false)
		{
			$this->course_stu_work_model->insert(
				array(
					'course_material_id' => $data['course_material_id'], 
					'user_id' => $data['user_id'], 
					'data' => Json::encode($data['data']),
				)
			);
		}
		else 
		{
			$this->course_stu_work_model->update(
				array(
					'data' => Json::encode($data['data']),
				),
				array(
					'course_material_id' => $data['course_material_id'],
					'user_id' => $data['user_id'],
				)
			);
		}
		
		/* Pegistered File Privileges */
		
		$privilege_course = array('confidential_level' => 'TA_ONLY', 'course_id' => $course_id);
		$privilege_owner = array('confidential_level' => 'PERSONAL', 'user_id' => $user_id);
		
		/* Save The two file privileges */
		
		$file_save_result = $this->stored_file_service->saveFile($data['data']['stored_file_id'], $privilege_course);
		if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
			return array('result' => false, 'reason' => array($file_save_result['reason']));
		$file_save_result = $this->stored_file_service->saveFile($data['data']['stored_file_id'], $privilege_owner);
		if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
			return array('result' => false, 'reason' => array($file_save_result['reason']));
		
		/* Delete old file privileges */
		
		if ($course_stu_work !== false)
		{
			$stu_work_data = Json::decode($course_stu_work['data'], true);
			$this->stored_file_service->deleteFile($stu_work_data['stored_file_id'], $privilege_course);
			$this->stored_file_service->deleteFile($stu_work_data['stored_file_id'], $privilege_owner);
		}
		
		return array('result' => true);
	}
	
	public function delete_by_course_material_id($course_material_id)
	{
		$course_material = $this->course_material_service->query_by_id($course_material_id);
		if ($course_material === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST));
		
		$course_id = $course_material['course_id'];
		
		$stu_work_result = $this->course_stu_work_model->query_stu_work_by_course_and_material($course_material_id, $course_id);
		
		/* Pegistered File Privileges */
		
		$privilege_course = array('confidential_level' => 'TA_ONLY', 'course_id' => $course_id);
		
		foreach ($stu_work_result as $stu_work)
		{
			$privilege_owner = array('confidential_level' => 'PERSONAL', 'user_id' => $stu_work['user_id']);
			
			$data = Json::decode($stu_work['data'], true);
			
			$file_delete_result = $this->stored_file_service->deleteFile($data['stored_file_id'], $privilege_course);
			if (!isset($file_delete_result['result']) || $file_delete_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_delete_result['reason']));	
			$file_delete_result = $this->stored_file_service->deleteFile($data['stored_file_id'], $privilege_owner);
			if (!isset($file_delete_result['result']) || $file_delete_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_delete_result['reason']));
		}
		
		$this->course_stu_work_model->delete(array('course_material_id' => $course_material_id));
		
		return array('result' => true);
	}
	
	public function query_stu_work_by_course_and_material($course_material_id, $course_id)
	{
		$stu_work_result = $this->course_stu_work_model->query_stu_work_by_course_and_material($course_material_id, $course_id);
		$stu_work_list = array();
		foreach ($stu_work_result as $stu_work)
			array_push($stu_work_list, $stu_work);
		return $stu_work_list;
	}
}

?>