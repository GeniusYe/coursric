<?php

namespace CourseInfo\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Json\Json;
use CourseInfo\Model\CourseInfo as CourseInfoModel;
use CourseInfo\Util\ServiceUtil;
use CourseInfo\Util\ErrorType;

class CourseInfoService
{
	private $db_adapter;
	private $course_info_model;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
		$this->course_info_model = new CourseInfoModel($this->db_adapter);
	}
	
	public function query_by_id($id)
	{
		$course_info_result = $this->course_info_model->select(array('course_id' => $id));
		$course_info = $course_info_result->current();
		
		return $course_info;
	}
	
	public function insert($id, $data)
	{
		$this->course_info_model->insert(
			array(
				'course_id' => $id,
				'data' => Json::encode(
					array(
						'name' => $data['name'],
						'goals' => '',
						'contents' => '',
						'news' => ''
					)
				),
				'svn_status' => 0,
			)
		);
		
		return array('result' => true);
	}
	
	/**
	 * 
	 * @param unknown $id
	 * @param unknown $data
	 * 		last_schedule_modify
	 * 		data
	 * 			news
	 * 			goals
	 * 			contents
	 * @return multitype:boolean
	 */
	public function update ($id, $data)
	{
		$course_info = $this->query_by_id($id);
		if ($course_info === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_INFO_DOES_NOT_EXIST));

		$format = array(
			'last_schedule_modify' => array('type' => ServiceUtil::RAW),
			'svn_status' => array('type' => ServiceUtil::RAW),
			'data' => array(
				'type' => ServiceUtil::OBJ,
				'obj' => array(
					'name' => array('type' => ServiceUtil::RAW),
					'news' => array('type' => ServiceUtil::RAW),
					'add_news' => array('type' => ServiceUtil::PUSH_FRONT, 'name' => 'news'),
					'goals' => array('type' => ServiceUtil::RAW),
					'contents' => array('type' => ServiceUtil::RAW),
				),
			)
		);

		$update_data = ServiceUtil::generate_update_data($format, $data, $course_info);
		
		$this->course_info_model->update($update_data, array('course_id' => $id));
		
		return array('result' => true);
	}
}

?>