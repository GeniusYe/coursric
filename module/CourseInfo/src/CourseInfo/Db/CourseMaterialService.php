<?php

namespace CourseInfo\Db;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Json\Json;
use RdnUpload\Service\StoredFileService;
use CourseInfo\Model\CourseMaterial as CourseMaterialModel;
use CourseInfo\Util\ErrorType;

class CourseMaterialService
{
	private $db_adapter;
	private $course_stu_work_service;
	private $stored_file_service;
	
	public function __construct(DbAdapter $adapter, StoredFileService $stored_file_service)
	{
		$this->db_adapter = $adapter;
		$this->course_material_model = new CourseMaterialModel($this->db_adapter);
		$this->stored_file_service = $stored_file_service;
	}
	
	public function query_by_id($id)
	{
		$course_material_result = $this->course_material_model->select(array('id' => $id));
		$course_material = $course_material_result->current();
		return $course_material;
	}
	
	public function query_by_id_with_work_state($user_id, $id)
	{
		$course_material_result = $this->course_material_model->query_by_id_with_work_state($user_id, $id);
		$course_material = $course_material_result->current();
		return $course_material;
	}
	
	public function query_by_course_id($course_id)
	{
		$course_material_result = $this->course_material_model->select(array('course_id' => $course_id));
		$course_material_list = array();
		foreach ($course_material_result as $course_material)
			array_push($course_material_list, $course_material);
		return $course_material_list;
	}
	
	public function query_by_course_id_with_work_state($user_id, $course_id)
	{
		$course_material_result = $this->course_material_model->query_by_course_id_with_work_state($user_id, $course_id);
		$course_material_list = array();
		foreach ($course_material_result as $course_material)
			array_push($course_material_list, $course_material);
		return $course_material_list;
	}
	
	/**
	 * @param $data:
	 * 		course_id
	 * 		type
	 * 		need_upload
	 * 		data
	 * 			abstract
	 * 			publish_time
	 * 			deadlines
	 * 			instructors
	 * 			attachments
	 * 			
	 * 
	 * @return multitype:boolean multitype:unknown  |number
	 */
	public function insert($data)
	{
		$course_id = $data['course_id'];
		$type = $data['type'];
		
		$this->course_material_model->insert(array(
			'course_id' => $course_id,
			'type' => $type,
			'need_upload' => $data['need_upload'],
			'data' => Json::encode($data['data']),
		));
		
		$id = $this->course_material_model->getLastInsertValue();
		
		/* Stored File Privileges */
		
		$this->stored_file_service = $this->stored_file_service;
		if ($type === 4)
			$privilege = array('confidential_level' => 'COURSE_PUBLIC', 'course_id' => $course_id);
		else
			$privilege = array('confidential_level' => 'PUBLIC');

		/* Save The two file privileges */
		
		if (!empty($data['data']['abstract_md']))
		{
			$file_save_result = $this->stored_file_service->saveFile($data['data']['abstract_md'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
			
		foreach ($data['data']['attachments'] as $attachment)
		{
			$file_save_result = $this->stored_file_service->saveFile($attachment['id'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
		
		return array('result' => true, 'id' => $id);
	}
	
	/**
	 * @param $data:
	 * 		id
	 * 		data
	 * 		need_upload
	 * 		
	 * @return multitype:boolean multitype:unknown
	 */
	public function update($id, $data)
	{
		$course_material = $this->query_by_id($id);
		if ($course_material === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST));
		
		$course_id = $course_material['course_id'];
		$type = $course_material['type'];
		
		$this->course_material_model->update(array(
			'data' => Json::encode($data['data']),
			'need_upload' => $data['need_upload'],
		), array(
			'id' => $id,
		));
		
		/* Stored File Privileges */
		
		if ($type === 4)
			$privilege = array('confidential_level' => 'COURSE_PUBLIC', 'course_id' => $course_id);
		else
			$privilege = array('confidential_level' => 'PUBLIC');
		
		/* Save The two file privileges */
		
		if (!empty($data['data']['abstract_md']))
		{
			$file_save_result = $this->stored_file_service->saveFile($data['data']['abstract_md'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
			
		foreach ($data['data']['attachments'] as $attachment)
		{
			$file_save_result = $this->stored_file_service->saveFile($attachment['id'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
			
		/* Delete old file privileges */
		
		$ori_data = Json::decode($course_material['data'], true);
		
		if (!empty($ori_data['abstract_md']))
		{
			$file_save_result = $this->stored_file_service->deleteFile($ori_data['abstract_md'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
		
		foreach ($ori_data['attachments'] as $attachment)
		{
			$file_save_result = $this->stored_file_service->deleteFile($attachment['id'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
		
		return array('result' => true);
	}
	
	public function delete($id)
	{
		$course_material = $this->query_by_id($id);
		if ($course_material === false)
			return array('result' => false, 'reason' => array(ErrorType::COURSE_MATERIAL_DOES_NOT_EXIST));
		
		$this->course_material_model->delete(array('id' => $id));
		
		/* Registered File Privileges */
		
		$ori_data = Json::decode($course_material['data'], true);
		if ($course_material['type'] === 4)
			$privilege = array('confidential_level' => 'COURSE_PUBLIC', 'course_id' => $course_material['course_id']);
		else
			$privilege = array('confidential_level' => 'PUBLIC');
		
		/* Delete old file privileges */
		
		foreach ($ori_data['attachments'] as $attachment)
		{
			$file_save_result = $this->stored_file_service->deleteFile($attachment['id'], $privilege);
			if (!isset($file_save_result['result']) || $file_save_result['result'] !== true)
				return array('result' => false, 'reason' => array($file_save_result['reason']));
		}
		
		return array('result' => true);
	}
}

?>