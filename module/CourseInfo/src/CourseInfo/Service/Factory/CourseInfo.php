<?php

namespace CourseInfo\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CourseInfo\Db\CourseInfoService;

class CourseInfo implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$course_info_service = new CourseInfoService($db_adapter);
		return $course_info_service;
	}
}

?>