<?php

namespace CourseInfo\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CourseInfo\Db\CourseMaterialService;

class CourseMaterial implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$stored_file_service = $serviceLocator->get('stored_file_service');
		$course_material_service = new CourseMaterialService($db_adapter, $stored_file_service);
		return $course_material_service;
	}
}

?>