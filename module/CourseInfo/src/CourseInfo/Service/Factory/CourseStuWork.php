<?php

namespace CourseInfo\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CourseInfo\Db\CourseStuWorkService;

class CourseStuWork implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$course_material_service = $serviceLocator->get('course_material_service');
		$stored_file_service = $serviceLocator->get('stored_file_service');
		$course_stu_work_service = new CourseStuWorkService($db_adapter, $course_material_service, $stored_file_service);
		return $course_stu_work_service;
	}
}

?>