<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class Module
{
	public function onBootstrap(MvcEvent $e)
	{
		$eventManager		= $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		$services = $e->getApplication()->getServiceManager();
		if($services->has('text-cache'))
		{
			$eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'getPageCache'), -1000);
			$eventManager->attach(MvcEvent::EVENT_RENDER, array($this, 'savePageCache'), -10000);
		}

		ini_set('error_reporting', E_ALL);
		ini_set('display_errors', 'Off');
		set_error_handler(array(&$this, 'error_handler')); 
	}
	
	public static function error_handler($number, $string, $file, $line, $context)
	{
		error_log("Number: [$number] String [$string] File [$file] Line [$line]");
		
		header('HTTP/1.0 500 Internal Server Error');
	    header('STATUS : 500 Internal Server Error');
	    
	    exit;
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}
	
	public function getPageCache(MvcEvent $event)
	{
		$match = $event->getRouteMatch();
		if (!$match)
			return;
		
		$controller = $match->getParam('controller');
		$action	 = $match->getParam('action');
		$namespace  = $match->getParam('__NAMESPACE__');

		switch ($controller)
		{
		case 'Application\Controller\Index':
			// the page can be cached so lets check if we have a cache copy of it
			$cache = $event->getApplication()
				->getServiceManager()
				->get('text-cache');
			
			$cacheKey = $this->pageCacheKey($match);
			$data = $cache->getItem($cacheKey);
			if (null !== $data)
			{
				$response = $event->getResponse();
				$response->setContent($data);
				// When we return a response object we actually shortcut execution:
				// the action responsible for this page is not executed
				return $response;
			}
			break;
		}
	}

	public function savePageCache(MvcEvent $event)
	{
		$match = $event->getRouteMatch();
		if (!$match)
			return;

		$controller = $match->getParam('controller');
		$action	 = $match->getParam('action');
		$namespace  = $match->getParam('__NAMESPACE__');
		
		switch ($controller)
		{
		case 'Application\Controller\Index':
			$response = $event->getResponse();
			$data = $response->getContent();
			$cache = $event->getApplication()
				->getServiceManager()
				->get('text-cache');
			
			$cacheKey = $this->pageCacheKey($match);
			$cache->setItem($cacheKey, $data);
			break;
		}
	}
	
	protected function pageCacheKey(RouteMatch $match)
	{
		return 'pagecache_'  . str_replace('/', '-', $match->getMatchedRouteName()) . '_'  . md5(serialize($match->getParams()));
	}
}
