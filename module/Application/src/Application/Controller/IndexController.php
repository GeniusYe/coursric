<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Util\ControllerUtil;
use Application\Util\ErrorType;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		$course_service = $this->getServiceLocator()->get('course_service');

		$semester_list = $this->getServiceLocator()->get('config')['semester'];
		
		$courses = $course_service->query_by_semester($semester_list['now']);
		
		return array('courses' => $courses, 'semester_list' => $semester_list);
	}
	
	public function courseAction()
	{
		$semester = $this->params('id');
		if ($semester === null)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$course_service = $this->getServiceLocator()->get('course_service');
		
		$courses = $course_service->query_course_list_by_semester($semester);

		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true, 'courses' => $courses));
	}
	
	public function authorAction()
	{
		return;
	}
}
