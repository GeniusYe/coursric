$(document).ready(function()
{
});

function modify_course(id)
{
	window.location.href = "../query-one/" + id;
}

function del_course(id)
{
	showInfoDialog("We do not support course delete operation, because of complicated relationship, we recommend you change the name of the course to another one.");
}

function add_course(semester)
{
	window.location.href = "../query-one/?semester=" + semester;
}