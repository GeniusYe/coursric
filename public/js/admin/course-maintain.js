$(document).ready(function()
{
	$("#new_teacher_row input.name_input").bind("keydown", new_stuff_confirm_enter("teacher"));

	$("#new_ta_row input.name_input").bind("keydown", new_stuff_confirm_enter("ta"));

	$("#new_stu_row input.name_input").bind("keydown", new_stuff_confirm_enter("stu"));
	
	$("#course_form a[name='submit']").bind("click", function()
	{
		$("#course_form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.href = "../query/" + $semester; });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
	
	$("#new_teacher_row input.id_input").bind("keydown", new_stuff_id_enter("teacher"));
	
	$("#new_ta_row input.id_input").bind("keydown", new_stuff_id_enter("ta"));
	
	$("#new_stu_row input.id_input").bind("keydown", new_stuff_id_enter("stu"));
	
	$user_loading = "loading";
	
	$("#stuDialog div.modal-footer button[name='confirm']").bind("click", group_submit_stu);
});
	
function new_stuff_id_enter(type) 
{
	return function(e)
	{
		if (e.which == 13)
		{
			$("input.name_input").val("");
			query_user(type);
			$(this).parent().parent().find("input.name_input").focus();
		}
	};
}

function new_stuff_confirm_enter(type)
{
	return function(e)
	{
		if (e.which == 13)
		{
			if ($user_loading == "success")
			{
				$(this).parent().parent().find("a[name='submit']").trigger("click");
				$(this).parent().parent().find("input.id_input").focus();
			}
			else
				alert("Please wait a few seconds for the username to be loaded.");
		}
	};
}

function query_user(role)
{
	name = "#new_" + role + "_row";
	$user_loading = "loading";
	$(name + " input.name_input").val("Loading..");
	$.ajax({
		url: "../query-user/" + $(name + " input.id_input").val(),
		type: "GET",
		dataType: "json",
		success: function(data)
		{
			$user_loading = "success";
			$(name + " input.name_input").val(data.user_name);
		},
		error: function()
		{
			$user_loading = "fail";
			$(name + " input.name_input").val("Loading Failed.");
		}
	});
}

function add_teacher() { add_role("teacher"); event.preventDefault(); }

function add_ta() { add_role("ta"); event.preventDefault(); }

function add_stu() { add_role("stu"); event.preventDefault(); }

function add_role(role)
{
	name = "#new_" + role + "_row";
	if ($(name + " input.name_input").val() != "")
	{
		var $str = "<tr value='" + $(name + " input.id_input").val() + "'><td><i class='stuff-list-current hidden'>Cu</i><i class='stuff-list-deleting hidden'>De</i><i class='stuff-list-creating'>Cr</i></td>";
		$str = $str + "</td><td>" + $(name + " input.id_input").val() + "</td><td>" + $(name + " input.name_input").val() + "</td>";
		$str = $str + "<td><a href='javascript: remove_" + role + "(" + $(name + " input.id_input").val() + ");'>DEL</a></td></tr>";
		var $new_line = $($str);
		$new_line.insertAfter(name);
		
		$.ajax({
			url: "../set-role/",
			type: "POST",
			data: { course_id : $course_id, role : role, user_id : $(name + " input.id_input").val(), direction : 1 },
			dataType: "json",
			success: function(data)
			{
				var $row = $("#new_" + data.role + "_table tr[value='" + data.user_id + "']");
				$row.find("i").addClass("hidden");
				$row.find("i.stuff-list-current").removeClass("hidden");
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	}
}

function remove_teacher(id) { remove_role("teacher", id); return false; }

function remove_ta(id) { remove_role("ta", id); return false; }

function remove_stu(id) { remove_role("stu", id); return false; }

function remove_role(role, id)
{
	name = "#new_" + role + "_row";

	var $row = $("#new_" + role + "_table tr[value='" + id + "']");
	$row.find("i").addClass("hidden");
	$row.find("i.stuff-list-deleting").removeClass("hidden");
	
	$.ajax({
		url: "../set-role/",
		type: "POST",
		data: { course_id : $course_id, role : role, user_id : id, direction : 0 },
		dataType: "json",
		success: function(data)
		{
			$("#new_" + data.role + "_table tr[value='" + data.user_id + "']").remove();
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
}

function show_stu_dialog()
{
	$("#stuDialog").modal("show");
	event.preventDefault();
}

function group_submit_stu()
{
	$.ajax({
		url: '../set-stu/',
		data: { course_id : $course_id, stu_list : $("#stuDialog div.modal-body textarea[name='stu_list']").val() },
		type: 'POST',
		dataType: 'json',
		success: function()
		{
			showInfoDialog("@success", function() { window.location.reload(); });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
	event.preventDefault();
}