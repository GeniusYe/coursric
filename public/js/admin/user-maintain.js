$(document).ready(function()
{
	$("#user_form a[name='submit']").bind("click", function()
	{
		$("#user_form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.href = "../query/" + $group; });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
});
