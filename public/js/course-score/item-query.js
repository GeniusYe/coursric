$(document).ready(function()
{
});

function modify_score_item(id)
{
	window.location.href = "../query-one/" + id;
}

function del_score_item(id)
{
	showConfirmDialog("Please Confirm Your Delete Command!", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../delete/" + id,
			type: "POST",
			dataType: "json",
			success: function()
			{
				showInfoDialog("@success", function(){ window.location.reload(); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function score_score_item(id)
{
	window.location.href = "../../score/query/" + id;
}

function notify_score_item(id)
{
	showConfirmDialog("Your are going to send one mail to all the related teachers / teaching assistants and students of this course, please confirm!", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../notify/" + id,
			type: "POST",
			dataType: "json",
			success: function()
			{
				showInfoDialog("@success");
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function publish_score_item(id)
{
	showConfirmDialog("Your are going to publish the score and send one mail to all the related teachers / teaching assistants and students of this course, please confirm!", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../publish/" + id,
			type: "POST",
			dataType: "json",
			success: function()
			{
				showInfoDialog("@success", function(){ window.location.reload(); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function add_score_item(course_id)
{
	window.location.href = "../query-one/?course_id=" + course_id;
}

function query_all_score(course_id)
{
	window.location.href = "../../score/query-all/" + course_id;
}