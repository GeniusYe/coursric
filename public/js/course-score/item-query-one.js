$(document).ready(function()
{
	$("#score-item-form a[name='submit']").bind("click", function()
	{
		$("#score-item-form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.href = "../../item/query/" + $course_id; });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
});
