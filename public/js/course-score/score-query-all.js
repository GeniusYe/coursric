$(document).ready(function()
{
	$("#download-score-btn").bind("click", function()
	{
		var $csv_content = $('#score-table').table2CSV({
			delivery: 'value',
		});
		$("#download-score-btn").attr("download", "score_report.csv");
		$("#download-score-btn").attr("href", "data:text/csv;charset=utf-8," + encodeURIComponent($csv_content) + "\r\n");
	});
});