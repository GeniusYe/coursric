$(document).ready(function()
{
	$("#student-list-form input").bind("change", function()		
	{
		var $name = $(this).parent().parent().attr("name");
		$(this).attr("name", $name);
	});
	
	$("#student-list-form").ajaxForm(
	{
		url: "../update/" + $course_score_item_id,
		type: "POST",
		dataType: "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function()
		{
			showInfoDialog("@success", function(){ window.location.href = "../../item/query/" + $course_id; });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
	
	$("#input-multiple-dialog button[name='confirm']").bind("click", function(){
		var $ori_data = $("#input-multiple-textarea").val();
		var $line = $ori_data.split("\n", 300);
		$.each($line, function(index, value){
			if (value == "")
				return;
			var $part = value.split("\t", 2);
			var $id = $part[0].substring(0, 11);
			var $score = $part[1];
			var $ori_score = $("#student-list-form > table tr[name='" + $id + "'] input.score").val();
			if ($ori_score != $score)
			{
				$("#student-list-form > table tr[name='" + $id + "'] input.score").val($score);
				$("#student-list-form > table tr[name='" + $id + "'] input.score").trigger("change");
			}
		});
		$("#input-multiple-dialog").modal("hide");
		event.preventDefault();
	});
});

function showInputMultipleDialog()
{
	$("#input-multiple-dialog").modal("show");
}