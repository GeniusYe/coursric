$(document).ready(function()
{
	$("#icon-upload-form").ajaxForm({
		url: "../../../files/file/upload",
		type: "post",
		dataType: "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			$("#icon-upload-dialog").modal("hide");
			$.ajax({
				url: "../maintain/",
				type: "POST",
				dataType: "json",
				data: 
				{
					type: 'icon',
					icon_id: data.id,
					icon_name: data.name,
				},
				success: function(data)
				{
					$("#user-info-left > div > img").attr("src", "../../../files/file/download?id=" + data.data.icon_id);
					showInfoDialog("@success");
				}
			});
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
});

function startMaintain(type)
{
	if (type == "icon")
	{
		$("#icon-upload-dialog").modal("show");
	}
	else
	{
		$("#user-info > div[name='" + type + "'] td.modification-value").removeClass("hidden");
		$("#user-info > div[name='" + type + "'] a.start-maintain-button").addClass("hidden");
		$("#user-info > div[name='" + type + "'] a.maintain-button").removeClass("hidden");
	}
	event.preventDefault();
}

function cancelMaintain(type)
{
	$("#user-info > div[name='" + type + "'] td.modification-value").addClass("hidden");
	$("#user-info > div[name='" + type + "'] a.start-maintain-button").removeClass("hidden");
	$("#user-info > div[name='" + type + "'] a.maintain-button").addClass("hidden");
	event.preventDefault();
}

function submitMaintain(type)
{
	$data = {
		"type" : type,
	};
	
	if (type == "user_info")
	{
		$data["email"] = $("#user-info > div[name='user_info'] input[name='email']").val();
		$data["mobile"] = $("#user-info > div[name='user_info'] input[name='mobile']").val();
		$data["bbsid"] = $("#user-info > div[name='user_info'] input[name='bbsid']").val();
		$data["intro"] = $("#user-info > div[name='user_info'] textarea[name='intro']").val();
	}
	else if (type == "subscribe_method")
	{
		$data["email"] = $("#user-info > div[name='subscribe_method'] input[name='email']").val();
		$data["mobile"] = $("#user-info > div[name='subscribe_method'] input[name='mobile']").val();
	}
	
	showInfoDialog("@operating");
	$.ajax({
		url: "../maintain/",
		type: "POST",
		dataType: "json",
		data: $data,
		success: function(data)
		{
			showInfoDialog("@success");
			$("#user-info > div[name='" + data.data.type + "'] td.modification-value").addClass("hidden");
			$("#user-info > div[name='" + data.data.type + "'] a.start-maintain-button").removeClass("hidden");
			$("#user-info > div[name='" + data.data.type + "'] a.maintain-button").addClass("hidden");
			if (data.data.type == "user_info")
			{
				$("#user-info > div[name='" + data.data.type + "'] td[name='email']").text(data.data.email);
				$("#user-info > div[name='" + data.data.type + "'] td[name='mobile']").text(data.data.mobile);
				$("#user-info > div[name='" + data.data.type + "'] td[name='bbsid']").text(data.data.bbsid);
				$("#user-info > div[name='" + data.data.type + "'] td[name='intro']").text(data.data.intro);
			}
			else if (data.data.type == "subscribe_method")
			{
				$("#user-info > div[name='" + data.data.type + "'] td[name='email']").text(data.data.email);
				$("#user-info > div[name='" + data.data.type + "'] td[name='mobile']").text(data.data.mobile);
			}
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
	event.preventDefault();
}
