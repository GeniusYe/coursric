$recaptcha_loaded = false;

$(document).ready(function()
{
	$("#resetPasswordDialog div.modal-footer > form > button[name='confirm']").bind("click", function()
	{
		var username = $("#resetPasswordForm input[name='username']").val();
		if (username > 0)
		{
			$("#resetPasswordForm").ajaxSubmit({
				url: "../../../authentication/password/resetapply/",
				type: "POST",
				dataType: "json",
				success: function(data)
				{
					if (data.result == true)
						window.location.href = "../../password/reset-apply-success/";
					else
						showInfoDialog("@error, There maybe some connecting problem with google recaptcha, please wait for a while.");
					refresh_recaptcha();
				},
				error: function(data)
				{
					if (data.responseJSON.reason[0] == 'RECAPTCHA_NOT_CORRECT')
						showInfoDialog("Sorry, you have input the wrong recaptcha code, please refresh and try again.");
					else
						showInfoDialog("@error " + data.responseJSON.reason);
					refresh_recaptcha();
				}
			});
		}
		else
			alert('Please input username first.');
		
		event.preventDefault();
	});
	$("#resetPasswordDialog div.modal-footer > form > button[name='cancel']").bind("click", function()
	{
		$("#resetPasswordDialog").modal("hide");
		event.preventDefault();
	});
});

function reset_apply()
{
	$("#resetPasswordDialog").modal("show");
}
