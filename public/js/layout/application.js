$(document).ready(function()
{
	$("#wechat_icon").popover({ 
		html : true, 
		content : "<img src='" + $base_path + "/img/code.jpg' />",
		placement : "top",
	});
	
	$.ajax({
		url: $base_path + "/authentication/log/status/",
		type: "POST",
		dataType: "json",
		data: { },
		success: function(data)
		{
			$("#navbar > ul > li.public_role").fadeIn();
			
			if (data.user == null)
			{
				$("#navbar > ul.navbar-right > li[name='login']").fadeIn();
				$("#navbar > ul > li.not_authenticated").fadeIn();
			}
			else
			{
				$("#navbar > ul.navbar-right > li[name='account']").fadeIn();
				
				if (data.admin == true)
					$("#navbar > ul > li[name='admin']").fadeIn();
				
				var $user_data = JSON.parse(data.user.data);
				if ($user_data['icon'] != undefined && $user_data['icon'] != "")
					$("#account-bar > a").html("<img src='" + $base_path + "/files/file/download?id=" + $user_data['icon'] + "' />	" + data.user.name);
				else
					$("#account-bar > a").html("<img src='" + $base_path + "/img/icon/fake-user-icon.png' />	" + data.user.name);
			}
		}
	});
});