$(document).ready(function()
{
	$("#semester_select").val($semester_now);
	
	$("#semester_select").bind("change", function(){
		$.ajax({
			url: $base_path + "/application/index/course/" + $(this).val(),
			type: "GET",
			dataType: "json",
			success: function(data)
			{
				var $content = $("#course > div[name='course_content']");
				$content.empty();
				$.each(data.courses, function()
				{
					var $course = $("<a href=\"course_info/query/introduction/" + this.id + "\" class=\"item\">" + this.name + "&nbsp;<span class=\"course-subtitle\">" + this.subname + "</span></a>");
					$content.append($course);
				});
			}
		});
	});
});