$(document).ready(function()
{
	$("a[data-toggle='tooltip']").tooltip();
	
	if ($("#upload-work-form").length > 0)
	{
		$("#upload-work-form").ajaxForm({
			url: "../../../files/file/upload",
			type: "post",
			dataType: "json",
			beforeSubmit: function()
			{
				$course_material_id = $("#upload-work-form input[name='course_material_id']").val();
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@operating");
				$.ajax({
					url: "../../stu_work/upload/",
					type: "POST",
					dataType: "json",
					data: { "file_id": data.id, "course_material_id": $course_material_id },
					success: function()
					{
						$("#upload-work-dialog").modal("hide");
						showInfoDialog("@success", function(){ window.location.reload(); });
					},
					error: function(data)
					{
						showInfoDialog("@error " + data.responseJSON.reason);
					}
				});
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	}
});

function modify_material(id)
{
	window.location.href = "../../maintain/query-one/" + id;
}

function del_material(id)
{
	showConfirmDialog("Please Confirm Your Delete Command!", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../../maintain/delete/" + id,
			type: "POST",
			dataType: "json",
			success: function()
			{
				showInfoDialog("@success", function(){ window.location.reload(); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function notify_material(id)
{
	showConfirmDialog("Your are going to send one mail to all the related teachers / teaching assistants and students of this course, please confirm!", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../../maintain/notify/" + id,
			type: "POST",
			dataType: "json",
			success: function()
			{
				showInfoDialog("@success");
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function check_stu_work(id)
{
	window.location.href = "../../stu_work/query/" + id;
}

function open_upload_work_dialog(course_material_id, file_id)
{
	$("#upload-work-form input[name='course_material_id']").val(course_material_id);
	if (file_id == null)
	{
		$("#upload-work-form a[name='old_file']").attr("href", "#");
		$("#upload-work-form a[name='old_file']").text("");
	}
	else
	{
		$("#upload-work-form a[name='old_file']").attr("href", "../../../files/file/download?name=work&id=" + file_id);
		$("#upload-work-form a[name='old_file']").text("Submitted File");
	}
	$("#upload-work-dialog").modal("show");
	event.preventDefault();
}

function add_material(course_id)
{
	window.location.href = "../../maintain/query-one/?course_id=" + course_id;
}

function show_detail(id)
{
	window.location.href = "../schedule-one/" + id;
}