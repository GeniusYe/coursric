$(document).ready(function()
{
	$("#setting-area div[name='svn_password']").css("display", "none");
	
	$("#setting-area div[name='svn_password'] form").ajaxForm({
		url : "../../setting/svn-password/" + $course_id,
		type : "POST",
		dataType : "json",
		beforeSubmit: function()
		{
			if ($("#setting-area div[name='svn_password'] form input[name='password']").val() != $("#setting-area div[name='svn_password'] form input[name='password_confirm']").val())
			{
				alert("Password confirmation is not correct!");
				return false;
			}
			else
				$("#reset-form").submit();
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			$("#setting-area div[name='svn_password']").slideUp();
			showInfoDialog("@success");
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
});

function reset_svn_password()
{
	$("#setting-area div[name='svn_password']").slideToggle();
}

function establish_svn_repository()
{
	$.ajax({
		url : "../../setting/establish-svn/" + $course_id,
		type : "POST",
		dataType : "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			showInfoDialog("@success", function(){ window.location.reload(); });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
}

function refresh_svn_repository()
{
	$.ajax({
		url : "../../setting/refresh-svn/" + $course_id,
		type : "POST",
		dataType : "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			showInfoDialog("@success", function(){ window.location.reload(); });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
}
