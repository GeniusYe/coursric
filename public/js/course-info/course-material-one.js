$(document).ready(function()
{
	if ($abstract_md != "")
	{
		$.ajax({
			url: "../../../files/file/download?id=" + $abstract_md,
			type: "GET",
			dataType: "html",
			success: function(data)
			{
				var converter = new Showdown.converter();
				var $converted_data = converter.makeHtml(data);
				var $md_display = $($converted_data);
				$("#md-div").empty();
				$("#md-div").append($md_display);
			}
		});
	}
	
	if ($material_type == "MT_5")
	{
		$("#video-div").empty();
		$.each($video_path, function(index, value){
			if (value != "")
			{
				$video = $("<div class='video-item'><video src='' /></div>");
				$video.find("video").attr("src", "../../../files/file/download?id=" + value);
				$("#video-div").append($video);
			}
		});
	}
});