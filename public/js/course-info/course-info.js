$(document).ready(function()
{
	$("#info-maintain-form").ajaxForm({
		url: "../../info_maintain/info/" + $course_id,
		type: "post",
		dataType: "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			$("#info-maintain-dialog").modal("hide");
			showInfoDialog("@success");

			if (data.type == 'add_news')
			{
				var $old_value = data.value + $(".course-info-content[name='news'] > p").text();
				$old_value = $old_value.replace(/\n/g, "<br />");
				$(".course-info-content[name='news'] > p").html("<p>" + $old_value + "</p>");
			}
			else
				$(".course-info-content[name='" + data.type + "'] > p").html("<p>" + data.value.replace(/\n/g, "<br />") + "</p>");
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
});

function showInfoMaintainDialog(type)
{
	if (type == 'add_news')
	{
		$("#info-maintain-form input[name='type']").val(type);
		$("#info-maintain-dialog h4.modal-title > span").text("MESSAGES");
		$("#info-maintain-form textarea[name='value']").val("");
	}
	else
	{
		$("#info-maintain-form input[name='type']").val(type);
		$("#info-maintain-dialog h4.modal-title > span").text($(".course-info-table-wrapper[name='" + type + "'] > h3.table-caption > strong").text());
		var $ori_data = $(".course-info-content[name='" + type + "'] > p").text();
		$("#info-maintain-form textarea[name='value']").val($ori_data);
	}
	$("#info-maintain-dialog").modal("show");
	event.preventDefault();
}