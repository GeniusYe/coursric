$(document).ready(function()
{
	$("#material_form select[name='type']").val($material_type);
	$("#material_form input[name='need_upload'][value='" + $need_upload + "']").attr("checked", "");
	
	$("#material_form select[name='type']").bind("change", function()
	{
		if ($(this).val() == 0 || $(this).val() == 5)
		{
			$("#material_form tr.stu_work").addClass("hidden");
		}
		else
		{
			$("#material_form tr").removeClass("hidden");
		}
	});
	$("#material_form select[name='type']").trigger("change");

	$("#file-upload-form").ajaxForm({
		url: "../../../files/file/upload",
		type: "post",
		dataType: "json",
		beforeSubmit: function()
		{
			showInfoDialog("@operating");
		},
		success: function(data)
		{
			$("#file-upload-dialog").modal("hide");
			showInfoDialog("@success");

			if ($file_upload_type == "attachment")
			{
				$newObj = $("<div><input type='text' name='attachment_t' class='form-control' value='' /><button class='btn btn-link' onclick='javascript:del_line(this);'>Del</button><div class='clear'></div>");
				$newObj.children("input[name='attachment_t']").val(data.name);
				$newObj.children("input[name='attachment_t']").attr("sources", data.id);
				$newObj.insertBefore($("#material-table button[name='attachment_add_btn']"));
			}
			else if ($file_upload_type == "abstract_md")
			{
				$("#material-table input[name='abstract_md']").val(data.id);
				$("#material-table a[name='abstract_md_uploaded_span']").removeClass("hidden");
				$("#material-table button[name='abstract_md_delete']").removeClass("hidden");
			}
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
	
	$("#material_form a[name='submit']").bind("click", function()
	{
		var $instructors = "";
		var $t = 0;
		$("#material_form input[name='instructor_t']").each(function()
		{
			if ($t == 1)
				$instructors = $instructors + "\n";
			else
				$t = 1;

			$instructors = $instructors + $(this).val();
		});
		$("#material_form input[name='instructor']").val($instructors);
		
		var $deadline = "";
		$t = 0;
		$("#material_form input[name='deadline_t']").each(function()
		{
			if ($t == 1)
				$deadline = $deadline + "\n";
			else
				$t = 1;

			$deadline = $deadline + $(this).val();
		});
		$("#material_form input[name='deadline']").val($deadline);
		
		var $attachment = "";
		$t = 0;
		$("#material_form input[name='attachment_t']").each(function()
		{
			if ($t == 1)
				$attachment = $attachment + "\n";
			else
				$t = 1;

			$attachment = $attachment + $(this).val() + "," + $(this).attr("sources");
		});
		
		$("#material_form input[name='attachment']").val($attachment);
		
		$("#material_form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.href = "../../query/schedule/" + $course_id; });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
});

function del_line(obj)
{
	$(obj).parent().remove();
	event.preventDefault();
}

function add_line(obj, type, name)
{
	$newObj = $("<div><input type='" + type + "' name='" + name + "' class='form-control' value='' /><button class='btn btn-link' onclick='javascript:del_line(this);'>Del</button><div class='clear'></div>");
	$newObj.insertBefore($(obj));
	event.preventDefault();
}

function add_attachment()
{
	$file_upload_type = "attachment";
	$("#file-upload-dialog").modal("show");
	event.preventDefault();
}

function upload_abstract_md()
{
	$file_upload_type = "abstract_md";
	$("#file-upload-dialog").modal("show");
	event.preventDefault();
}

function delete_abstract_md(obj)
{
	$(obj).parent().find("a[name='abstract_md_uploaded_span']").remove();
	$(obj).parent().find("input[name='abstract_md']").val("");
	$(obj).addClass("hidden");
	event.preventDefault();
}