<?php
return array(
	'user_group' => array(
		'detail' => array(
			'Undergraduate Student starting in year 2010' => 103,
			'Undergraduate Student starting in year 2011' => 113,
			'Undergraduate Student starting in year 2012' => 123,
			'Undergraduate Student starting in year 2013' => 133,
			'Undergraduate Student starting in year 2014' => 143,
		),
	),
);
