<?php
return array(
	'log_db' => array(
		'driver' => 'mysqli',
		'database' => 'coursric',
		'username' => 'coursric',
		'password' => '123456',
		'hostname' => 'localhost',
		'if_ssl' => false,
		'charset' => 'utf8',
		'options' => array(
			'buffer_results' => 1,
		),
	),
);
