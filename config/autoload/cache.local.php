<?php
return array(
	'cache' => array(
		'adapter' => array(
			'name' => 'filesystem',
			'options' => array(
				'cache_dir' => 'data/cache',
				'ttl' => 100,
			)
		),
		'plugins' => array(
			'exception_handler' => array(
				'throw_exceptions' => false,
			),
		),
	),
);