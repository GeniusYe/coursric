# Coursric #

This is a easy, strong and powerful software.
It provides a easy way to share course material, send notifications, collection student works, publicity student scores, gather evaluation of courses.


## Installation ##

#### Setup Zend ####
 
##### Using Composer (recommended) #####

The recommended way to install zend is the composer, try the following instructions 

    php composer.phar self-update
	php composer.phar install

to install `zend framework 2`

(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

#### Changes on Zend Framework ####

##### Mail #####

-	Zend/Db/Adapter/Driver/Mysqli/Connection.php
	-	support sql connection
-	Zend/Mail/Transport/Smtp.php
	-	support invalid mail address
-	Zend/Mvc/ResponseSender/HttpResponseSender.php
	-	support high performance big file download


#### Virtual Host ####

Afterwards, set up a virtual host to point to the public/ directory of the
project and you should be ready to go!

#### Initialize Config Files ####

All the config files are located at /config/auto_load, those are 

-	app_name.local.php
-	cache.local.php
-	database.local.php
-	log.local.php
-	mail.local.php
-	semester.local.php
-	svn.local.php
-	user_group.local.php

We need to config them one by one, according to our system.

###### app_name.local.php ######

	app_name' => array(
		//Name of your department
		'name_display' => 'Www.Etcss.Fudan.Edu.Cn',
		//Domain of your department
		'domain_name' => 'www.etcss.fudan.edu.cn',
	),

###### cache.local.php ######

	'cache' => array(
		'adapter' => array(
			'name' => 'filesystem',
			'options' => array(
				// Directory of your cache file
				'cache_dir' => 'data/cache',
				'ttl' => 100,
			)
		),
		'plugins' => array(
			'exception_handler' => array(
				'throw_exceptions' => false,
			),
		),
	),

###### database.local.php ######

	'db' => array(
		'driver' => 'mysqli',
		'database' => 'genuine_space',
		'username' => 'genuine_space',
		'password' => '123456',
		'hostname' => 'localhost',
		'options' => array(
			'buffer_results' => 1,
		),
	)

###### log.local.php ######

By default, log is logged into database, but we can assign different database connections to it.

	'log_db' => array(
		'driver' => 'mysqli',
		'database' => 'genuine_space',
		'username' => 'genuine_space',
		'password' => '123456',
		'hostname' => 'localhost',
		'options' => array(
			'buffer_results' => 1,
		),
	),

###### mail.local.php ######

We should define a mail box to send notifications to teachers, teaching assistants and students

	'mail' => array(
		'name'			  => 'mail.fudan.edu.cn',
		'host'			  => 'mail.fudan.edu.cn',
		'port'				=> 25,		
		'connection_class'  => 'login',
		'connection_config' => array(
			'username' => '------',
			'password' => '------',
		),
	)

###### svn.local.php ######

This system support an automantic svn management system, we should define the directory of our svn repositories

	'svn' => array(
		'directory' => 'H:/svn',
		//template file position, with 1.8.8 subversion, you can keep this default
		'template_directory' => 'data/templates/svn',
		'svn_root' => 'C:/Program Files (x86)/Subversion/bin',
	)

###### semester.local.php ######

To keep our system on date, we need to add additional semester to it.

Its format is `name => semester_id`, `now` points to the current one

	'semester' => array(
		'now' => 8,
		'detail' => array(
			'2013-2014 the 2nd semester' => 8,
			'2013-2014 the 1st semester' => 7,
		),
	),

###### user_group.local.php ######

We can define our own user group here.

	'user_group' => array(
		'detail' => array(
			'Undergraduate Student starting in year 2010' => 103,
			'Undergraduate Student starting in year 2011' => 113,
			'Undergraduate Student starting in year 2012' => 123,
			'Undergraduate Student starting in year 2013' => 133,
		),
	),

#### Initialize Directories ####

Make sure you have created the following directory:

- cache file directory, default one is `data/cache`
- file upload directory, default one is `data/upload` 
- svn template directory, default one is 'data/template/svn`
- svn repository directory

#### Recaptcha ####

- Make sure your computer can access www.coursric.org

